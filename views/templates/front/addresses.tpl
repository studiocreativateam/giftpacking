<div class="half_box gift-packing-delivery-table" style="display: none">
  <h3>{l s='Choose' mod='giftpacking'}</h3>
  <div class="radio-wrapper">
    {foreach $giftPackingDeliveryAddresses as $giftPackingDeliveryAddress}
    <div class="radio-choose">
      {if !empty($giftPackingDeliveryAddress.address_name)}
      <div class="form flexible-modal" data-action="choose_giftpacking_delivery_address" data-id_giftpacking_delivery_group="{$giftPackingDeliveryAddress.id}" data-ajax="true" data-group_no="">
        <input type="radio" name="gift-packing-delivery-address-select" value="{$giftPackingDeliveryAddress.id}" class="gift-packing-delivery-address-select form-value" {if isset($giftPackingDeliverySelected) &&
          $giftPackingDeliverySelected==$giftPackingDeliveryAddress.id} checked {/if}>

      </div>
      {/if}

      <div class="form" data-action="set_giftpacking_delivery_address_name" data-id_giftpacking_delivery_group="{$giftPackingDeliveryAddress.id}" data-ajax="true">

        <input type="text" class="gift-packing-delivery-address-text form-value" placeholder="{l s='Main address' mod='giftpacking'}" value="{$giftPackingDeliveryAddress.address_name}">
      </div>

      <div class="form flexible-modal" data-action="delete_giftpacking_delivery" data-id_giftpacking_delivery_group="{$giftPackingDeliveryAddress.id}" data-ajax="true">
        <button type="button" class="btn btn-sm btn-giftpacking-delivery-delete"><i class="icon-close"></i></button>
      </div>
    </div>
    {/foreach}
  </div>
</div>
