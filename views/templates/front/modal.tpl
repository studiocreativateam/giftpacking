<div class="modal" id="deliveryAddressesModal" tabindex="-1" role="dialog" aria-labelledby="deliveryAddressesModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="deliveryAddressesModalLabel">{l s='Addresses' mod='giftpacking'}</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="icon-close"></i>
        </button>
      </div>
      <div class="modal-body flexwrapper">
        {include file="module:giftpacking/views/templates/front/addresses.tpl"}
        <div class="half_box gift-packing-delivery-address-table">
          {include file="module:giftpacking/views/templates/front/address.tpl"}
        </div>
        <div class="buttons centered">
          <div class="btn smallbtn btn-approve-delivery-address disabled">{l s='Approve' mod='giftpacking'}</div>
          <div class="btn smallbtn transparent btn-add-delivery-address"><i class="icon-plus"></i>{l s='Add delivery address' mod='giftpacking'}</div>
        </div>
      </div>
    </div>
  </div>
</div>
