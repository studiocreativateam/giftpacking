
<div class="col-address">
  <div class="form" data-action="set_giftpacking_delivery_address_country" {if !empty($giftPackingDeliveryAddress)}data-id_giftpacking_delivery_group="{$giftPackingDeliveryAddress.id}"{/if} data-ajax="true">
    <label class="mobile-visible">{l s='Country' mod='giftpacking'}*</label>
    <input type="text" class="gift-packing-delivery-address-country-text form-value" placeholder="{l s='Country' mod='giftpacking'}*" {if !empty($giftPackingDeliveryAddress)}value="{$giftPackingDeliveryAddress.address_country}"{/if}>
  </div>

  <div class="form" data-action="set_giftpacking_delivery_address_city" {if !empty($giftPackingDeliveryAddress)}data-id_giftpacking_delivery_group="{$giftPackingDeliveryAddress.id}"{/if} data-ajax="true">
    <label class="mobile-visible">{l s='City' mod='giftpacking'}*</label>
    <input type="text" class="gift-packing-delivery-address-city-text form-value" placeholder="{l s='City' mod='giftpacking'}*" {if !empty($giftPackingDeliveryAddress)}value="{$giftPackingDeliveryAddress.address_city}"{/if}>
  </div>

  <div class="form" data-action="set_giftpacking_delivery_address_zipcode" {if !empty($giftPackingDeliveryAddress)}data-id_giftpacking_delivery_group="{$giftPackingDeliveryAddress.id}"{/if} data-ajax="true">
    <label class="mobile-visible">{l s='Zipcode' mod='giftpacking'}*</label>
    <input type="text" class="gift-packing-delivery-address-zipcode-text form-value" placeholder="{l s='Zipcode' mod='giftpacking'}*" {if !empty($giftPackingDeliveryAddress)}value="{$giftPackingDeliveryAddress.address_zipcode}"{/if}>
  </div>

  <div class="form" data-action="set_giftpacking_delivery_address_street" {if !empty($giftPackingDeliveryAddress)}data-id_giftpacking_delivery_group="{$giftPackingDeliveryAddress.id}"{/if} data-ajax="true">
    <label class="mobile-visible">{l s='Street' mod='giftpacking'}*</label>
    <input type="text" class="gift-packing-delivery-address-street-text form-value" placeholder="{l s='Street' mod='giftpacking'}*" {if !empty($giftPackingDeliveryAddress)}value="{$giftPackingDeliveryAddress.address_street}"{/if}>
  </div>

  <div class="form" data-action="set_giftpacking_delivery_address_apartment_no" {if !empty($giftPackingDeliveryAddress)}data-id_giftpacking_delivery_group="{$giftPackingDeliveryAddress.id}"{/if} data-ajax="true">
    <label class="mobile-visible">{l s='Apartment no.' mod='giftpacking'}*</label>
    <input type="text" class="gift-packing-delivery-address-apartment-no-text form-value" placeholder="{l s='Apartment no.' mod='giftpacking'}*" {if !empty($giftPackingDeliveryAddress)}value="{$giftPackingDeliveryAddress.address_apartment_no}"{/if}>
  </div>

  <div class="form" data-action="set_giftpacking_delivery_name_and_surname" {if !empty($giftPackingDeliveryAddress)}data-id_giftpacking_delivery_group="{$giftPackingDeliveryAddress.id}"{/if} data-ajax="true">
    <label class="mobile-visible">{l s='Name and surname' mod='giftpacking'}</label>
    <input type="text" class="gift-packing-delivery-address-text form-value" placeholder="{l s='Name and surname' mod='giftpacking'}" {if !empty($giftPackingDeliveryAddress)}value="{$giftPackingDeliveryAddress.name_and_surname}"{/if}>
  </div>

  <div class="form" data-action="set_giftpacking_delivery_email" {if !empty($giftPackingDeliveryAddress)}data-id_giftpacking_delivery_group="{$giftPackingDeliveryAddress.id}"{/if} data-ajax="true">
    <label class="mobile-visible">{l s='Email' mod='giftpacking'}</label>
    <input type="text" class="gift-packing-delivery-address-text form-value" placeholder="{l s='Email' mod='giftpacking'}" {if !empty($giftPackingDeliveryAddress)}value="{$giftPackingDeliveryAddress.email}"{/if}>
  </div>

  <div class="form" data-action="set_giftpacking_delivery_phone" {if !empty($giftPackingDeliveryAddress)}data-id_giftpacking_delivery_group="{$giftPackingDeliveryAddress.id}"{/if} data-ajax="true">
    <label class="mobile-visible">{l s='Phone' mod='giftpacking'}</label>
    <input type="text" class="gift-packing-delivery-address-text form-value" placeholder="{l s='Phone' mod='giftpacking'}" {if !empty($giftPackingDeliveryAddress)}value="{$giftPackingDeliveryAddress.phone}"{/if}>
  </div>

  <div class="form" data-action="choose_giftpacking_delivery" {if !empty($giftPackingDeliveryAddress)}data-id_giftpacking_delivery_group="{$giftPackingDeliveryAddress.id}"{/if} data-ajax="true">
    <label class="mobile-visible">{l s='Delivery method' mod='giftpacking'}</label>
    <select class="gift-packing-delivery-select form-value">
      {foreach $delivers as $deliver}
      <option value="{$deliver.id}" {if !empty($giftPackingDeliveryAddress) && $deliver.id==$giftPackingDeliveryAddress.id_giftpacking_delivery} selected{/if}>
        {$deliver.title}
        ({Tools::displayPrice(Tools::convertPrice($deliver.price * ((Tools::getTaxRate($deliver.tax) + 100)/100.)))})
      </option>
      {/foreach}
    </select>
  </div>

</div>
