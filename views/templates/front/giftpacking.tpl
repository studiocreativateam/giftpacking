<div class="gift-packing" data-carrier_id="{$carrier_id}">
  <table cellpadding="0" cellspacing="0" border="0" width="100%" class="cart-table gift-packing-table table table-hover" data-has_products_without_packing_and_addresses="{$hasProductsWithoutPackingAndAddresses}">
    <thead>
      <tr>
        <th class="cart-left" width="70%">{l s='Product' mod='giftpacking'}</th>
        <th width="30%">{l s='Package' mod='giftpacking'}</th>
      </tr>
    </thead>
    <tbody>
      {foreach $items['products'] as $item}
      <tr data-product_id="{$item['product']['id_product']}">
        <td>
          <h5 style="display: inline;">
            <a href="{$link->getProductLink($item['product'])}" title="{$item['product']['name']}" target="_self">{$item['product']['name']}</a>
          </h5> (1 {l s='pcs' mod='giftpacking'}.)
        </td>
        <td>
          <select class="gift-packing-select">
            <option value="">{l s='Without packing' mod='giftpacking'}</option>
            {for $groupNo = 1; $groupNo <= $items['groups_count'] + 1; $groupNo++} <option value="{$groupNo}" {if $groupNo==$item['group_no']} selected {/if}>{l s='Gift no.' mod='giftpacking'} {$groupNo}</option>
              {/for}
          </select>
        </td>
      </tr>
      {/foreach}
    </tbody>
  </table>
  {if $groups}
  <div class="cart-table gift-packing-additional-info-table table table-hover">


    {foreach $groups as $group}
    <div class="box_gift" data-group_no="{$group['group']['group_no']}">

      <h5>
        {l s='Gift no.' mod='giftpacking'} {$group['group']['group_no']}
      </h5>



      <label class="gift_choose">{l s='Wybierz wygląd' mod='giftpacking'}</label>
      <div class="form" data-action="choose_giftpacking" data-group_no="{$group['group']['group_no']}" data-ajax="true">

        <a class="zoomimg" href="{$images[$group['group']['id_giftpacking']]}" data-lightbox="image-1"><i class="icon-szukaj"></i></a>
        <select name="package-select" class="gift-packing-appearance-select form-value">
          {foreach $packages as $package}
          <option value="{$package['id']}" {if $group['group']['id_giftpacking']==$package['id']} selected{/if}>{$package['title']}
            ({Tools::displayPrice(Tools::convertPrice($package['price'] * ((Tools::getTaxRate($package['tax']) + 100)/100.)))})
          </option>
          {/foreach}
        </select>
      </div>

      <ul>
        {foreach $group['products'] as $groupProduct}
        <li>{$groupProduct['product_name']} ({$groupProduct['quantity']} {l s='pcs' mod='giftpacking'}.)</li>
        {/foreach}
      </ul>

      <div class="form" data-action="update_message_gift" data-group_no="{$group['group']['group_no']}" data-ajax="true">
        <input type="text" value="{$group['group']['message']}" class="form-value gift-packing-message-text" placeholder="{l s='Write something as a gift' mod='giftpacking'}...">
      </div>


      <div class="form flexible" data-action="set_status_different_delivery_address_giftpacking" data-group_no="{$group['group']['group_no']}" data-ajax="true">

        <input type="checkbox" id="checkbox-{$group['group']['group_no']}" class="different_delivery_address form-value" {if !empty($group['group']['different_delivery_address'])} checked{/if}>
        <label for="checkbox-{$group['group']['group_no']}">{l s='Different delivery address' mod='giftpacking'}</label>
      </div>
      {if !empty($group['group']['different_delivery_address'])}
      <div class="form flexible changeaddress">
        <span class="current-address">
          {if empty($group['group']['address_name'])}
          {l s='Delivery address not selected' mod='giftpacking'}
          {else}
          <div>
            {$group['group']['delivery_title']} <strong>({Tools::displayPrice(Tools::convertPrice($group['group']['delivery_price'] * ((Tools::getTaxRate($group['group']['delivery_tax']) + 100)/100.)))})</strong>
          </div>
          {$group['group']['address_name']}
          {/if}</span>
        <div class="btn btn-sm smallbtn btn-choose-delivery-address" data-group_no="{$group['group']['group_no']}" data-id_giftpacking_delivery_group="{$group['group']['id_giftpacking_group_delivery']}">{l s='Choose address' mod='giftpacking'}</div>
      </div>
      {/if}
    </div>

    {/foreach}

  </div>

  {/if}
</div>
