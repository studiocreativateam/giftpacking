<script>
    window.gift_packing_translations = {$translations|json_encode nofilter};
    window.gift_packing_psv = {$psv};
    window.gift_packing_images = {$images|json_encode nofilter};
    window.gift_packing_delivery_images = {$deliveryImages|json_encode nofilter};
</script>