{if !empty($admin_giftpackinggroups)}
    <div class="table-responsive panel {if $hook == 'hookDisplayAdminOrderMain'}card{/if}">
        <h2 class="panel-heading {if $hook == 'hookDisplayAdminOrderMain'}card-header{/if}">{l s='Gift packing' mod='giftpacking'}</h2>
        {if $hook == 'hookDisplayAdminOrderMain'}
        <div class="card-body">
            {/if}
            <table class="table" id="gift_wrapping_table">
                <thead>
                <tr>
                    <th><span class="title_box ">{l s='Gift no.' mod='giftpacking'}</span></th>
                    <th><span class="title_box ">{l s='Package' mod='giftpacking'}</span></th>
                    <th><span class="title_box ">{l s='Delivery' mod='giftpacking'}</span></th>
                    <th><span class="title_box ">{l s='Delivery address' mod='giftpacking'}</span></th>
                    <th><span class="title_box ">{l s='Products' mod='giftpacking'}</span></th>
                    <th><span class="title_box ">{l s='Message' mod='giftpacking'}</span></th>
                </tr>
                </thead>
                <tbody>
                {foreach from=$admin_giftpackinggroups item=gift}
                    <tr>
                        <td>{$gift.group_no}</td>
                        <td>{$gift.giftpacking_title} ({if $gift.giftpacking_price != 0}{$gift.giftpacking_price}{else}{l s='Free' mod='giftpacking'}{/if})
                        </td>
                        <td>{$gift.giftpacking_delivery_title} ({if $gift.giftpacking_delivery_price != 0}{$gift.giftpacking_delivery_price}{else}{l s='Free' mod='giftpacking'}{/if})
                        </td>
                        <td>{$gift.delivery_address}</td>
                        <td>
                        {if isset($gift.products) && $gift.products}
                            {foreach from=$gift.products item=gift_product}
                                {$gift_product.product_name} ({l s='Ref' mod='giftpacking'}: {$gift_product.reference}) ({$gift_product.quantity} {l s='pcs' mod='giftpacking'}.)<br />
                            {/foreach}
                        {/if}
                        </td>
                        <td>{$gift.message}</td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
            {if $hook == 'hookDisplayAdminOrderMain'}
        </div>
        {/if}
    </div>
{/if}