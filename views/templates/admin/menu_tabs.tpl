<div class="col-lg-2">
    <div class="list-group">
        {foreach from=$tabs key=tab_key item=tab}
            <a
                    class="list-group-item {if $tab_key == $active_tab || ($active_tab == '' && $tab_key == 'generel_settings')}active{/if}"
                    href="{$module_url|escape:'htmlall':'UTF-8'}&{$module_tab_key}={$tab_key|escape:'htmlall':'UTF-8'}">
                {if isset($tab.icon)}
                    <i class="{$tab.icon}"></i>
                {/if}
                {if $tab_key != 'version'}
                    {$tab.title|escape:'htmlall':'UTF-8'}
                {else}
                    {$tab.title|escape:'htmlall':'UTF-8'} - {$module_version|escape:'html':'UTF-8'}
                {/if}
            </a>
        {/foreach}
        <a
                style="margin-top:30px; display: inline-block"
                class="list-group-item"
                href="https://studio-creativa.pl">
            <img class="col-lg-12" src="{$logo_url}" alt="Studio Creativa sp. z o.o. Logo">
        </a>
    </div>
</div>
