{**
* 2012 - 2020 HiPresta
*
* MODULE Advanced gift wrapping
*
* @author    HiPresta <support@hipresta.com>
* @copyright HiPresta 2020
* @license   Addons PrestaShop license limitation
* @link      https://hipresta.com
*
* NOTICE OF LICENSE
*
* Don't use this module on several shops. The license provided by PrestaShop Addons
* for all its modules is valid only once for a single shop.
*}
aaaaaaaaaaaaaaaaaaaaaaa
{extends file="helpers/list/list_content.tpl"}
{block name="td_content"}
	{if $key == 'status'}
		<a data-id = {$tr.id|escape:'htmlall':'UTF-8'} data-status = {$tr.active|escape:'htmlall':'UTF-8'}  class="status btn {if $tr.active == '0'}btn-danger{else}btn-success{/if}" 
		href="#" title="{if $tr.active == '0'}{l s='Disabled' mod='hiadvancedgiftwrapping'}{else}{l s='Enabled' mod='hiadvancedgiftwrapping'}{/if}">
			<i class="{if $tr.active == '0'}icon-remove {else}icon-check{/if}"></i>
		</a>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}
