{**
* 2012 - 2020 HiPresta
*
* MODULE Advanced gift wrapping
*
* @author    HiPresta <support@hipresta.com>
* @copyright HiPresta 2020
* @license   Addons PrestaShop license limitation
* @link      https://hipresta.com
*
* NOTICE OF LICENSE
*
* Don't use this module on several shops. The license provided by PrestaShop Addons
* for all its modules is valid only once for a single shop.
*}

{extends file="helpers/form/form.tpl"}
{block name="field"}
	{if $input.type == 'block_search_product'}
		<div id="gift_block_products" class="search_product">
			<div>
				<input type="hidden" name="inputBlockProducts" id="inputBlockProducts" value="{$products_id}" />
				<div class="col-lg-6">
						<input type="text" id="product_search" name="product_search" value="" autocomplete="off" class="ac-input">
				</div>
				<div class="col-lg-2">
					<button type="button" id="add-gift-product" class="btn btn-default" name="add-gift-products">
						<i class="icon-plus-sign-alt"></i> {l s='Add' mod='hiadvancedgiftwrapping'}
					</button>
				</div>
				<div id="gift_products" class="col-lg-12 col-lg-offset-3">
					{foreach from=$product_content item=product}
						<div class="form-control-static">
							<button type="button" class="btn btn-default delete_gift_product" data-id-product="{$product['id_product']}">
								<i class="icon-remove text-danger"></i>
							</button>
							{$product['id_product']|intval} - {$product['name']|escape:'html':'UTF-8'}
						</div>
					{/foreach}
				</div>
			</div>
			<script type="text/javascript">
				$(document).ready(function(){
					$('#product_search').autocomplete(agwp_admin_controller_dir+"&ajax=1", 
				    {
				        minChars: 2,
				        max: 50,
				        width: 500,
				        formatItem: function (data) {
				            return data[0]+ '. '+data[2] + '-' + data[1];
				        },
				        scroll: false,
				        multiple: false,
				        extraParams: {
				            action : 'product_search',
				            id_lang : id_lang,
				            secure_key : agwp_secure_key,
				        }
				    });
				});
			</script>
		</div>
	{/if}
	{$smarty.block.parent}
{/block}
