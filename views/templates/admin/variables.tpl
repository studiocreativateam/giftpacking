<script type="text/javascript">
    {literal}
        var psv = {/literal}{$psv|floatval}{literal};
        var id_lang = {/literal}{$id_lang|intval}{literal};
        var sc_gift_packing_secure_key = '{/literal}{$sc_gift_packing_secure_key}{literal}';
        var sc_gift_packing_admin_controller_dir = {/literal}{$sc_gift_packing_admin_controller_dir|json_encode nofilter}{literal};
        var address_token = '{/literal}{getAdminToken tab='AdminAddresses'}{literal}';
    {/literal}
</script>