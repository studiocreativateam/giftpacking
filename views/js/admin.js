function update_helper_list(action) {
    $.ajax({
        type: 'POST',
        dataType: "JSON",
        url: sc_gift_packing_admin_controller_dir['neutral'],
        data: {
            ajax: true,
            action: action,
            secure_key: sc_gift_packing_secure_key,
            psv: psv,
        },
        success: function (response) {
            $("#form-giftpacking, #form-giftpacking_delivery").replaceWith(response.content);
            updateDeleteButtons();
        }
    });
}
function updateDeleteButtons() {
    $("#form-giftpacking .delete, #form-giftpacking_delivery .delete").each(function () {
        const $btn = $(this);
        const id = $btn.attr("onclick").match(/id=([0-9]+)/)[1];
        $btn.attr('data-id', id);
        $btn.removeAttr('onclick');
    });
}
function show_form(th, action, action_type, id) {
    $.ajax({
        type: 'POST',
        dataType: "JSON",
        url: sc_gift_packing_admin_controller_dir['neutral'],
        data: {
            ajax: true,
            action: action,
            action_type: action_type,
            id: id,
            secure_key: sc_gift_packing_secure_key,
            psv: psv,
        },
        beforeSend: function () {
            if (th.hasClass("edit")) {
                th.find('i').removeClass('icon-pencil').addClass('icon-refresh icon-spin');
            } else {
                th.find('i').removeClass('process-icon-new').addClass('process-icon-refresh icon-spin');
            }
        },
        success: function (response) {
            if (th.hasClass("edit")) {
                th.find('i').removeClass('icon-refresh icon-spin').addClass('icon-pencil');
            } else {
                th.find('i').removeClass('process-icon-refresh icon-spin').addClass('process-icon-new');
            }
            $("#modal_form .content").html(response.content);
            $('#modal_form').modal('show');
        }
    });
}

function save_form(form, action, helper_action) {
    var formdata = new FormData($(form)[0])
    formdata.append("action", action);
    formdata.append("secure_key", sc_gift_packing_secure_key);
    formdata.append("psv", psv);
    formdata.append("ajax", true);
    $.ajax({
        type: 'POST',
        dataType: "JSON",
        url: sc_gift_packing_admin_controller_dir[action],
        data: formdata,
        contentType: false,
        processData: false,
        beforeSend: function () {
            form.find('i.process-icon-save').removeClass('process-icon-save').addClass('process-icon-refresh icon-spin');
        },
        success: function (response) {
            form.find('i.process-icon-refresh').removeClass('process-icon-refresh icon-spin').addClass('process-icon-save');
            if (response.error != '') {
                showErrorMessage(response.error);
            } else {
                showSuccessMessage('Successful Save');
                $('#modal_form').modal('hide');
                update_helper_list(helper_action);
            }
        }
    });
    return false;
}

function delete_list_item(th, action, id) {
    $.ajax({
        type: 'POST',
        dataType: "JSON",
        url: sc_gift_packing_admin_controller_dir['neutral'],
        data: {
            ajax: true,
            action: action,
            id: id,
            secure_key: sc_gift_packing_secure_key,
            psv: psv,
        },
        success: function (response) {
            if (response.error != '') {
                showErrorMessage(response.error);
            } else {
                showSuccessMessage(response.success);
                th.closest('tr').remove();
            }
        }
    });
}

$(document).ready(function () {

    $(document).on('click', '#desc-giftpacking-new', function (e) {
        e.preventDefault();
        show_form($(this), 'show_form', 'add', '');
    });

    $(document).on('click', '#desc-giftpacking_delivery-new', function (e) {
        e.preventDefault();
        show_form($(this), 'show_delivery_form', 'add', '');
    });

    $(document).on('submit', '#modal_form form#configuration_form', function (e) {
        e.preventDefault();
        if ($('.modal form[action*="giftpacking=delivery"]').length > 0) {
            save_form($(this), 'save_delivery', 'update_helper_delivery');
        } else if ($('.modal form[action*="giftpacking=list"]').length > 0) {
            save_form($(this), 'save_list', 'update_helper_list');
        }
    });

    $(document).on('click', '.giftpacking .edit', function (e) {
        e.preventDefault();
        const $btn = $(this);
        const id = $btn.attr("href").match(/id=([0-9]+)/)[1];
        show_form($btn, 'show_form', 'update', id);
    });

    $(document).on('click', '.giftpacking_delivery .edit', function (e) {
        e.preventDefault();
        const $btn = $(this);
        const id = $btn.attr("href").match(/id=([0-9]+)/)[1];
        show_form($btn, 'show_delivery_form', 'update', id);
    });

    $(document).on('click', '.giftpacking .delete', function (e) {
        e.preventDefault();
        const $btn = $(this);
        const id = $btn.attr("data-id");
        delete_list_item($btn, 'delete_list_item', id);
    });

    $(document).on('click', '.giftpacking_delivery .delete', function (e) {
        e.preventDefault();
        const $btn = $(this);
        const id = $btn.attr("data-id");
        delete_list_item($btn, 'delete_delivery_item', id);
    });

    updateDeleteButtons();
});