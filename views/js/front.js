let packingGroups = [];
let giftPackages = [];

const carrierInput = document.getElementById('delivery_option_' + document.getElementsByClassName('gift-packing')[0].getAttribute('data-carrier_id'));
carrierInput.closest('.delivery-option').style.display = 'none';
setInterval(function () {
    if (carrierInput.closest('.delivery-option').style.display !== 'none') {
        carrierInput.closest('.delivery-option').style.display = 'none';
    }
}, 100);

$(document).ready(function () {
    const $giftPacking = $('.gift-packing');
    const $deliveryAddressesModal = $('#deliveryAddressesModal');
    const $carrierInput = $(carrierInput);
    $carrierInput.prop('disabled', true);
    select2Refresh();
    updateShipping();
    updatePackingGroups();
    $(document).on('change', 'select.gift-packing-select', function () {
        $giftPacking.css('opacity', '0.3');
        updatePackingGroups();
        ajaxUpdateGiftPacking();
    }).on('change', 'select.gift-packing-appearance-select, .gift-packing-delivery-select, .gift-packing-delivery-address-text, .gift-packing-delivery-address-country-text, .gift-packing-delivery-address-city-text, .gift-packing-delivery-address-zipcode-text, .gift-packing-delivery-address-street-text, .gift-packing-delivery-address-apartment-no-text, .gift-packing-message-text', function () {
        $giftPacking.css('opacity', '0.3');
        const $form = $(this).closest('div.form');
        $deliveryAddressesModal.find('.gift-packing-delivery-table').css('opacity', 0.3).find('input').prop('disabled', true);
        updateApproveBtn();
        sendGiftPackingForm($form, function () {
            prestashop.emit('updateCart', {
                reason: 'update',
            });
            $deliveryAddressesModal.find('.gift-packing-delivery-table').css('opacity', 1).find('input').prop('disabled', true);
        });
    }).on('click', '.btn-giftpacking-delivery-delete', function () {
        $giftPacking.css('opacity', '0.3');
        $deliveryAddressesModal.find('.modal-body').css('opacity', 0.3);
        const $form = $(this).closest('div.form');
        sendGiftPackingForm($form, function () {
            prestashop.emit('updateCart', {
                reason: 'update',
            });
            console.log('krok1');
            $form.closest('.radio-choose').remove();
            ajaxGetGiftPackingAddress();
            $deliveryAddressesModal.find('.modal-body').css('opacity', 1);
        });
    }).on('change', 'input.different_delivery_address', function () {
        $giftPacking.css('opacity', '0.3');
        const $form = $(this).closest('div.form');
        sendGiftPackingForm($form, function () {
            prestashop.emit('updateCart', {
                reason: 'update',
            });
        });
    }).on('click', '.btn-add-delivery-address', function () {
        $deliveryAddressesModal.find('.modal-body').css('opacity', 0.3);
        $.ajax({
            url: prestashop.urls.base_url + 'index.php?fc=module&module=giftpacking&controller=packing',
            type: 'POST',
            cache: false,
            data: {
                ajax: true,
                action: 'add_delivery_address',
                giftpacking_delivery_selected: $deliveryAddressesModal.find('div.form[data-action="choose_giftpacking_delivery_address"] input[type="radio"]:checked').closest('.form').attr('data-id_giftpacking_delivery_group')
            },
            success: function (result) {
                const data = JSON.parse(result);
                if (data.success) {
                    if (data.content) {
                        let html = '';
                        $(data.content).each(function () {
                            const $this = $(this);
                            if ($this.html()) {
                                html += this.outerHTML;
                            }
                        });
                        $deliveryAddressesModal.find('.gift-packing-delivery-table').replaceWith($(html));
                        $deliveryAddressesModal.find('div.form[data-action="choose_giftpacking_delivery_address"]').attr('data-group_no', $deliveryAddressesModal.attr('data-group_no'));
                        select2Refresh();
                    }
                    $deliveryAddressesModal.find('.modal-body').css('opacity', 1);

                    ajaxGetGiftPackingAddress();
                }
            }
        });
    }).on('click', '.btn-choose-delivery-address', function () {
        const $btn = $(this);
        $deliveryAddressesModal.attr('data-group_no', $btn.attr('data-group_no'));
        $deliveryAddressesModal.find('div.form[data-action="choose_giftpacking_delivery_address"]').find('input[type="radio"]').prop('checked', false);
        $deliveryAddressesModal.find('div.form[data-action="choose_giftpacking_delivery_address"]').each(function () {
            const $radioBtn = $(this);
            $radioBtn.attr('data-group_no', $btn.attr('data-group_no'));
            if ($radioBtn.find('input[type="radio"]').val() === $btn.attr('data-id_giftpacking_delivery_group')) {
                $radioBtn.find('input[type="radio"]').prop('checked', true);
            }
        });
        ajaxGetGiftPackingAddress();
        $deliveryAddressesModal.modal('show');
    }).on('change', '.gift-packing-delivery-address-select', ajaxGetGiftPackingAddress)
        .on('click', '.btn-approve-delivery-address:not(.disabled)', function () {
            $giftPacking.css('opacity', '0.3');
            const $form = $deliveryAddressesModal.find('div.form[data-action="choose_giftpacking_delivery_address"] input[type="radio"]:checked').closest('.form');
            sendGiftPackingForm($form, function () {
                prestashop.emit('updateCart', {
                    reason: 'update',
                });
            });
            $deliveryAddressesModal.modal('hide');
        });

    function sendGiftPackingForm($form, callback) {
        const data = {};
        $.each($form[0].attributes, function (i, a) {
            if (a.name.indexOf('data-') !== -1) {
                data[a.name.replace('data-', '')] = a.value;
            }
        });
        if ($form.find('input[type="checkbox"].form-value').length) {
            data['value'] = $form.find('input[type="checkbox"].form-value').is(':checked');
        } else {
            data['value'] = $form.find('.form-value').val();
        }
        $.ajax({
            url: prestashop.urls.base_url + 'index.php?fc=module&module=giftpacking&controller=packing',
            type: 'POST',
            cache: false,
            data: data,
            success: function (result) {
                const data = JSON.parse(result);
                if (data.success) {
                    if (data.content) {
                        let html = '';
                        $(data.content).each(function () {
                            const $this = $(this);
                            if ($this.html()) {
                                html += $this.html();
                            }
                        });
                        $giftPacking.html(html);
                        select2Refresh();
                    }
                    $giftPacking.css('opacity', 1);
                    if (callback) {
                        callback();
                    }
                }
            }
        });
    }

    function updatePackingGroups() {
        packingGroups = [];
        const $selects = $('select.gift-packing-select');
        $selects.each(function () {
            const $select = $(this);
            const value = $select.val().length > 0 && !isNaN($select.val()) ? parseInt($select.val()) : null;
            if (value && !packingGroups.includes(value)) {
                packingGroups.push(value);
            }
        });
        const maxValue = packingGroups.length > 0 ? packingGroups.reduce(function (a, b) {
            return Math.max(a, b);
        }) : 0;
        $selects.each(function () {
            const $select = $(this);
            const currentValue = $select.val().length > 0 && !isNaN($select.val()) ? parseInt($select.val()) : null;
            $select.html('');
            $select.append(new Option(gift_packing_translations.without_packing, '', false, false));
            for (let a = 1; a <= maxValue + 1; a++) {
                $select.append(new Option(gift_packing_translations.gift_no + ' ' + a, a, a === currentValue, a === currentValue));
            }
        });
        if (packingGroups.length) {
            setTimeout(function () {
                $(document).trigger('has-giftpacking-groups');
            }, 1000);
        }
    }

    function ajaxUpdateGiftPacking() {
        setGiftPackagesData();
        $.ajax({
            url: prestashop.urls.base_url + 'index.php?fc=module&module=giftpacking&controller=packing',
            type: 'POST',
            cache: false,
            data: {
                ajax: true,
                action: 'set_gift_packing',
                giftPackages: giftPackages
            },
            success: function (result) {
                const data = JSON.parse(result);
                if (data.success) {
                    prestashop.emit('updateCart', {
                        reason: 'update',
                    });
                }
            }
        });
    }

    function ajaxGetGiftPacking() {
        $.ajax({
            url: prestashop.urls.base_url + 'index.php?fc=module&module=giftpacking&controller=packing',
            type: 'POST',
            cache: false,
            data: {
                ajax: true,
                action: 'get_gift_packing',
                giftpacking_delivery_selected: $deliveryAddressesModal.find('div.form[data-action="choose_giftpacking_delivery_address"] input[type="radio"]:checked').closest('.form').attr('data-id_giftpacking_delivery_group')
            },
            success: function (result) {
                const data = JSON.parse(result);
                if (data.success) {
                    let htmlMain = '';
                    let htmlModal = '';
                    $(data.content.main).each(function () {
                        const $this = $(this);
                        if ($this.html()) {
                            htmlMain += $this.html();
                        }
                    });
                    $(data.content.modal).each(function () {
                        const $this = $(this);
                        if ($this.html()) {
                            htmlModal += this.outerHTML;
                        }
                    });
                    $deliveryAddressesModal.find('.gift-packing-delivery-table').replaceWith($(htmlModal));
                    $deliveryAddressesModal.find('div.form[data-action="choose_giftpacking_delivery_address"]').attr('data-group_no', $deliveryAddressesModal.attr('data-group_no'));
                    $giftPacking.html(htmlMain);
                    $giftPacking.css('opacity', 1);
                    select2Refresh();
                    updateShipping();
                    setTimeout(function () {
                        const $radio = $('.gift-packing-delivery-address-select');
                        if ($radio.length === 1) {
                            $radio.prop('checked', true).trigger('change');
                        }
                    }, 500);
                }
            }
        });
    }

    function ajaxGetGiftPackingAddress() {
        console.log('krok2');
        $.ajax({
            url: prestashop.urls.base_url + 'index.php?fc=module&module=giftpacking&controller=packing',
            type: 'POST',
            cache: false,
            data: {
                ajax: true,
                action: 'get_gift_packing_address',
                giftpacking_delivery_selected: $deliveryAddressesModal.find('div.form[data-action="choose_giftpacking_delivery_address"] input[type="radio"]:checked').closest('.form').attr('data-id_giftpacking_delivery_group')
            },
            success: function (result) {
                const data = JSON.parse(result);
                if (data.success) {
                    let htmlAddress = '';
                    $(data.content.address).each(function () {
                        const $this = $(this);
                        if ($this.html()) {
                            htmlAddress += $this.html();
                        }
                    });
                    const $table = $deliveryAddressesModal.find('.gift-packing-delivery-address-table');
                    $table.html($(htmlAddress));
                    setTimeout(function () {
                        if ($('div[data-action="set_giftpacking_delivery_address_name"]').length === 0) {
                            $table.html('<div class="row text-center" style="margin:auto"><div class="col-12"><i class="fa fa-info"></i></div><div class="col-12">Nie dodano żadnego adresu</div></div>').removeClass('half_box');
                            $('.gift-packing-delivery-table').hide();
                            $('.btn-add-delivery-address').addClass('empty');
                        } else {
                            $table.addClass('half_box');
                            $('.gift-packing-delivery-table').show();
                            $('.btn-add-delivery-address').removeClass('empty');
                        }
                    }, 500);
                    $deliveryAddressesModal.find('input').prop('disabled', false);
                    select2Refresh();
                    updateApproveBtn();
                }
            }
        });
    }

    function setGiftPackagesData() {
        giftPackages = [];
        $('select.gift-packing-select').each(function () {
            const $select = $(this);
            const value = $select.val().length > 0 && !isNaN($select.val()) ? parseInt($select.val()) : null;
            if (value !== null) {
                giftPackages.push({
                    product_id: $select.closest('tr').attr('data-product_id'),
                    group_no: value
                });
            }
        });
    }

    function select2Refresh() {
        $('.gift-packing select').select2({
            width: '100%',
        });
        $('.gift-packing-appearance-select').select2({
            width: '100%',
            templateResult: function (idioma) {
                if (idioma.id) {
                    return $("<span><img src='" + (gift_packing_images[idioma.id] || '') + "'/> " + idioma.text + "</span>");
                }
            },
            templateSelection: function (idioma) {
                if (idioma.id) {
                    return $("<span><img src='" + (gift_packing_images[idioma.id] || '') + "'/> " + idioma.text + "</span>");
                }
            }
        });
        $('.gift-packing-delivery-select').select2({
            width: '100%',
            dropdownParent: $deliveryAddressesModal,
            templateResult: function (idioma) {
                if (idioma.id) {
                    return $("<span><img src='" + (gift_packing_delivery_images[idioma.id] || '') + "'/> " + idioma.text + "</span>");
                }
            },
            templateSelection: function (idioma) {
                if (idioma.id) {
                    return $("<span><img src='" + (gift_packing_delivery_images[idioma.id] || '') + "'/> " + idioma.text + "</span>");
                }
            }
        });
    }

    function updateShipping() {
        const hasProductsWithoutPacking = 1 === parseInt($giftPacking.find('.gift-packing-table').attr('data-has_products_without_packing_and_addresses'));
        $('.cart-summary-line#cart-subtotal-shipping').toggle(hasProductsWithoutPacking);
        $('.block-shipping').toggle(hasProductsWithoutPacking);
        $('.delivery-options-list input[type="radio"]').prop('disabled', !hasProductsWithoutPacking);
        if (!hasProductsWithoutPacking && $('.delivery-options-list input[type="radio"]:checked').length && !carrierInput.checked) {
            $('.delivery-options-list input[type="radio"]').each(function () {
                const $radio = $(this);
                if ($radio.is(':checked')) {
                    $radio.prop('checked', false).trigger('change');
                }
            });
            $('.pudo-map-container').toggle(hasProductsWithoutPacking);
        }
        $carrierInput.prop('disabled', hasProductsWithoutPacking);
        if (carrierInput.checked === hasProductsWithoutPacking) {
            $carrierInput.prop('checked', !hasProductsWithoutPacking).trigger('change');
        }
    }

    function updateApproveBtn() {
        const $btn = $('.btn-approve-delivery-address');
        let disabled = false;
        $('.gift-packing-delivery-address-country-text, .gift-packing-delivery-address-city-text, .gift-packing-delivery-address-zipcode-text, .gift-packing-delivery-address-street-text, .gift-packing-delivery-address-apartment-no-text').each(function () {
            if (!$(this).val().length) {
                disabled = true;
            }
        });
        $btn.toggleClass('disabled', disabled);
    }

    if (gift_packing_psv >= 1.7) {
        prestashop.on('updatedCart', function (event) {
            if (event.eventType === 'updateCart') {
                $giftPacking.css('opacity', 0.3);
                ajaxGetGiftPacking();
            }
        })
    }
});
