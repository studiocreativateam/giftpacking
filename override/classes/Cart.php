<?php

class Cart extends CartCore
{
    public function getGiftWrappingPrice($with_taxes = true, $id_address = null)
    {
        $giftPacking = Module::getInstanceByName('giftpacking');
        if ($giftPacking && $giftPacking->active) {
            return $giftPacking->calculateGiftPackingFees($this->id) + $giftPacking->calculateGiftPackingShippingCost();
        } else {
            return parent::getGiftWrappingPrice($with_taxes, $id_address);
        }
    }

    public function getTotalShippingCost($delivery_option = null, $use_tax = true, Country $default_country = null)
    {
        $giftPacking = Module::getInstanceByName('giftpacking');
        if ($giftPacking && $giftPacking->active && !empty($giftPacking->calculateGiftPackingFees($this->id))) {
            Context::getContext()->cart->gift = true;
        }
        return parent::getTotalShippingCost($delivery_option, $use_tax, $default_country);
    }
}
