<?php

class Tools extends ToolsCore
{
    public function getTaxRate($taxGroupId)
    {
        return TaxRulesGroup::getAssociatedTaxRatesByIdCountry(Context::getContext()->country->id)[$taxGroupId] ?? 0;
    }
}
