<?php

class Ps_CashondeliveryOverride extends Ps_Cashondelivery
{
    public function hookPaymentOptions($params)
    {
        $giftPacking = Module::getInstanceByName('giftpacking');
        if ($giftPacking && $giftPacking->active) {
            if ($giftPacking->hasAnyProduct($this->context->cart->id)) {
                return ;
            }
        }
        return parent::hookPaymentOptions($params);
    }
}