# Prestashop module GiftPacking 😎 

![drawing](logo.png?raw=true)

## About
A module to add a gift packing on your web store.
### Usage
`{hook h="displayGiftPacking"}`

## Contributing
Studio Creativa Sp. z o.o. Copyright (c) permanent

![drawing](views/img/sc-logo.png?raw=true)

## Requirements
Require PrestaShop e-commerce solution.