<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class giftpacking extends Module
{
    public $psv;
    public $giftPackingModule;
    public $errors = [];
    public $success = [];
    public $image_width;
    public $image_height;

    public function __construct()
    {
        $this->name = 'giftpacking';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'studio-creativa.pl';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = [
            'min' => '1.7',
            'max' => _PS_VERSION_
        ];
        $this->bootstrap = true;
        $this->secure_key = Tools::hash($this->name);

        parent::__construct();

        $this->displayName = $this->l('Gift packing');
        $this->description = $this->l('Gift packing.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        require_once _PS_MODULE_DIR_ . $this->name . '/classes/GiftPackingModel.php';
        require_once _PS_MODULE_DIR_ . $this->name . '/classes/GiftPackingDeliveryModel.php';
        require_once _PS_MODULE_DIR_ . $this->name . '/classes/GiftPackingModule.php';
        require_once _PS_MODULE_DIR_ . $this->name . '/classes/GiftPackingRepository.php';

        $this->giftPackingModule = new GiftPackingModule($this);

        $this->globalVars();
    }

    public function install()
    {
        $success =  parent::install() &&
            $this->installDb() &&
            $this->registerHook('displayGiftPacking') &&
            $this->registerHook('actionValidateOrder') &&
            $this->registerHook('displayAdminOrder') &&
            $this->registerHook('displayAdminOrderMain') &&
            $this->giftPackingModule->createSubmenu();

        $id_carrier = (int)Configuration::get('SC_GIFT_PACKING_CARRIER_ID');
        $carrier = Carrier::getCarrierByReference((int)$id_carrier);
        if (empty($carrier) || empty($id_carrier)) {
            $carrier = new Carrier();
            $carrier->name = 'Wszystkie produkty wysłane inną metodą';
            $carrier->shipping_method = 1;
            $carrier->is_free = 1;
            $carrier->shipping_handling = 1;
            $carrier->shipping_external = 1;
            $carrier->shipping_method = 1;
            $carrier->max_width = 0;
            $carrier->max_height = 0;
            $carrier->max_depth = 0;
            $carrier->max_weight = 0;
            $carrier->grade = 0;
            $carrier->is_module = 1;
            $carrier->need_range = 1;
            $carrier->range_behavior = 1;
            $carrier->external_module_name = $this->name;

            $delay = [];
            foreach (Language::getLanguages(true) as $language) {
                $delay[$language['id_lang']] = $this->l('Wszystkie produkty wysłane inną metodą');
            }
            $carrier->delay = $delay;
            $carrier->save();
            foreach (Zone::getZones() as $zone) {
                $carrier->addZone($zone['id_zone']);
            }
            foreach (Language::getLanguages(true, false, true) as $language) {
                $carrier->setGroups(array_map(function ($group) {
                    return $group['id_group'];
                }, Group::getGroups($language)));
            }
            Configuration::updateValue('SC_GIFT_PACKING_CARRIER_ID', $carrier->id);
        }

        return $success;
    }

    public function uninstall()
    {
        $success = parent::uninstall() &&
            $this->deleteDb() &&
            $this->giftPackingModule->deleteSubmenu();

        $id_carrier = (int)Configuration::get('SC_GIFT_PACKING_CARRIER_ID');
        $carrier = Carrier::getCarrierByReference((int)$id_carrier);
        $carrier && $carrier->delete();

        return $success;
    }

    private function installDb()
    {
        $res = Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'giftpacking` (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `active` TINYINT  NOT NULL,
                `price` varchar(100) NOT NULL,
                `tax` varchar(100) NOT NULL,
                `image` text NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'giftpacking_lang` (
                `id` int(10) unsigned NOT NULL,
                `id_lang` int(10) unsigned NOT NULL,
                `title` varchar(255) NOT NULL,
                PRIMARY KEY (`id`,`id_lang`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'giftpacking_delivery` (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `active` TINYINT  NOT NULL,
                `price` varchar(100) NOT NULL,
                `tax` varchar(100) NOT NULL,
                `logo` text NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'giftpacking_delivery_lang` (
                `id` int(10) unsigned NOT NULL,
                `id_lang` int(10) unsigned NOT NULL,
                `title` varchar(255) NOT NULL,
                PRIMARY KEY (`id`,`id_lang`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'giftpacking_shop` (
                `id_giftpacking` int(10) unsigned NOT NULL,
                `id_shop` int(10) unsigned NOT NULL,
                PRIMARY KEY (`id_giftpacking`,`id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'giftpacking_delivery_shop` (
                `id_giftpacking_delivery` int(10) unsigned NOT NULL,
                `id_shop` int(10) unsigned NOT NULL,
                PRIMARY KEY (`id_giftpacking_delivery`,`id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'giftpacking_group` (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `id_cart` int(10) NOT NULL,
                `group_no` int(10) NOT NULL,
                `id_giftpacking` int(10) NULL,
                `different_delivery_address` int(1) NOT NULL DEFAULT 0,
                `id_giftpacking_group_delivery` int(10) NULL,
                `message` varchar(255) NULL,
                PRIMARY KEY (`id`, `id_cart`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'giftpacking_group_delivery` (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `id_cart` int(10) NOT NULL,
                `id_giftpacking_delivery` int(10) NULL,
                `address_name` varchar(255) NULL,
                `address_country` varchar(255) NULL,
                `address_city` varchar(255) NULL,
                `address_zipcode` varchar(255) NULL,
                `address_street` varchar(255) NULL,
                `address_apartment_no` varchar(255) NULL,
                `name_and_surname` varchar(255) NULL,
                `email` varchar(255) NULL,
                `phone` varchar(255) NULL,
                PRIMARY KEY (`id`, `id_cart`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'giftpacking_group_product` (
                `id_giftpacking_group` int(10) unsigned NOT NULL,
                `id_product` int(10) NOT NULL
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        return $res;
    }

    private function deleteDb()
    {
        $db_drop = [
            'giftpacking',
            'giftpacking_lang',
            'giftpacking_delivery',
            'giftpacking_delivery_lang',
            'giftpacking_shop',
            'giftpacking_delivery_shop',
            'giftpacking_group',
            'giftpacking_group_delivery',
            'giftpacking_group_product',
        ];
        foreach ($db_drop as $value) {
            DB::getInstance()->Execute('DROP TABLE IF EXISTS ' . _DB_PREFIX_ . pSQL($value));
        }
        return true;
    }

    public function hookDisplayGiftPacking($params)
    {
        if (!$this->context->controller->ajax) {
            $this->context->controller->addCSS($this->_path . 'views/css/lightbox.css');
            $this->context->controller->addCSS($this->_path . 'views/css/front.css');
            $this->context->controller->addJS($this->_path . 'views/js/lightbox.js');
            $this->context->controller->addJS($this->_path . 'views/js/front.js');
        }
        $this->context->smarty->assign([
            'translations' => $this->getTranslations(),
            'psv' => $this->psv,
            'deliveryImages' => $this->getDeliveryImages(),
            'images' => $this->getImages(),
            'giftPackingDeliveryAddresses' => GiftPackingRepository::getAddressForCart($this->context->cart->id),
            'delivers' => GiftPackingDeliveryModel::getAllList(true),
        ]);
        return $this->display(__FILE__, 'views/templates/front/variables.tpl') .
            $this->displayGiftPacking() .
            $this->display(__FILE__, 'views/templates/front/modal.tpl');
    }

    public function hookActionValidateOrder(array $params)
    {
        /** @var Order $order */
        $order = $params['order'];
        $message = $this->getMessage($order->id_cart);
        if (!empty($message)) {
            $orderCustomer = new Customer($order->id_customer);
            $customerThread = new CustomerThread();
            if (isset($order->id_customer)) {
                $customerThread->id_customer = $orderCustomer->id;
            }
            if (isset($orderCustomer->email)) {
                $customerThread->email = $orderCustomer->email;
            } else {
                $customerThread->email = '-';
            }

            $customerThread->id_contact = 0;
            $customerThread->id_lang = $this->context->language->id;
            $customerThread->id_shop = $order->id_shop;
            $customerThread->id_order = $order->id;
            $customerThread->status = 'open';
            $customerThread->token = Tools::passwdGen(12);
            $customerThread->save();
            $orderMessage = new CustomerMessage();
            $orderMessage->id_customer_thread = $customerThread->id;
            $orderMessage->message = $message;
            $orderMessage->private = true;
            $orderMessage->save();
        }
    }

    public function hookDisplayAdminOrder($params)
    {
        $giftPackingGroups = GiftPackingRepository::getGiftGroupsForCart($params['cart']->id);
        $taxes = TaxRulesGroup::getAssociatedTaxRatesByIdCountry($this->context->country->id);

        foreach ($giftPackingGroups as $key => $giftPackingGroup) {
            $giftPackingModel = GiftPackingModel::getAllOrderGiftByid($giftPackingGroup['id_giftpacking']);
            $giftPackingGroupDelivery = GiftPackingRepository::getGiftPackingGroupDeliveryById($giftPackingGroup['id_giftpacking_group_delivery'], $params['cart']->id);
            $giftPackingDeliveryModel = new GiftPackingDeliveryModel($giftPackingGroupDelivery['id_giftpacking_delivery'], $this->context->language->id);
            $packingTax = $taxes[$giftPackingModel['tax'] ?? 0] ?? 0;
            $packageCost = $this->context->currentLocale->formatPrice(Tools::convertPrice(($giftPackingModel['price'] ?? 0) * (($packingTax + 100) / 100.)), $this->context->currency->iso_code);
            $giftPackingGroups[$key]['giftpacking_title'] = $giftPackingModel['title'];
            $giftPackingGroups[$key]['giftpacking_price'] = $packageCost;
            $deliveryTax = $taxes[$giftPackingDeliveryModel->tax ?? 0] ?? 0;
            $deliveryCost = $this->context->currentLocale->formatPrice(Tools::convertPrice(($giftPackingDeliveryModel->price ?? 0) * (($deliveryTax + 100) / 100.)), $this->context->currency->iso_code);
            $giftPackingGroups[$key]['giftpacking_delivery_title'] = $giftPackingDeliveryModel->title;
            if (empty($giftPackingGroup['different_delivery_address']) || $giftPackingGroup['different_delivery_address'] == 0) {
                $giftPackingGroups[$key]['delivery_address'] = $this->l('Main delivery address');
                $deliveryCost = 0;
            } else {
                $giftPackingGroups[$key]['delivery_address'] = trim(preg_replace('/\s+/', ' ', "{$giftPackingGroupDelivery['address_country']} {$giftPackingGroupDelivery['address_city']} {$giftPackingGroupDelivery['address_street']} {$giftPackingGroupDelivery['address_zipcode']} {$giftPackingGroupDelivery['address_apartment_no']}"));
            }
            $giftPackingGroups[$key]['giftpacking_delivery_price'] = $deliveryCost;
            $giftPackingGroups[$key]['products'] = GiftPackingRepository::getProductsForGiftPackingGroup($giftPackingGroup['id']);
        }

        $this->context->smarty->assign([
            'admin_giftpackinggroups' => $giftPackingGroups,
            'hook' => 'hookDisplayAdminOrderMain'
        ]);

        return $this->display(__FILE__, 'admin_giftpacking_list.tpl');
    }

    public function hookDisplayAdminOrderMain($params)
    {
        $order = new Order($params['id_order']);

        if (!Validate::isLoadedObject($order)) {
            return;
        }

        $giftPackingGroups = GiftPackingRepository::getGiftGroupsForCart($order->id_cart);
        $taxes = TaxRulesGroup::getAssociatedTaxRatesByIdCountry($this->context->country->id);
        foreach ($giftPackingGroups as $key => $giftPackingGroup) {
            $giftPackingModel = GiftPackingModel::getAllOrderGiftByid($giftPackingGroup['id_giftpacking']);
            $giftPackingGroupDelivery = GiftPackingRepository::getGiftPackingGroupDeliveryById($giftPackingGroup['id_giftpacking_group_delivery'], $order->id_cart);
            $giftPackingDeliveryModel = new GiftPackingDeliveryModel($giftPackingGroupDelivery['id_giftpacking_delivery'], $this->context->language->id);
            $packingTax = $taxes[$giftPackingModel['tax'] ?? 0] ?? 0;
            $packageCost = $this->context->currentLocale->formatPrice(Tools::convertPrice(($giftPackingModel['price'] ?? 0) * (($packingTax + 100) / 100.)), $this->context->currency->iso_code);
            $giftPackingGroups[$key]['giftpacking_title'] = $giftPackingModel['title'];
            $giftPackingGroups[$key]['giftpacking_price'] = $packageCost;
            $deliveryTax = $taxes[$giftPackingDeliveryModel->tax ?? 0] ?? 0;
            $deliveryCost = $this->context->currentLocale->formatPrice(Tools::convertPrice(($giftPackingDeliveryModel->price ?? 0) * (($deliveryTax + 100) / 100.)), $this->context->currency->iso_code);
            $giftPackingGroups[$key]['giftpacking_delivery_title'] = $giftPackingDeliveryModel->title;
            if (empty($giftPackingGroup['different_delivery_address']) || $giftPackingGroup['different_delivery_address'] == 0) {
                $giftPackingGroups[$key]['delivery_address'] = $this->l('Main delivery address');
                $deliveryCost = 0;
            } else {
                $giftPackingGroups[$key]['delivery_address'] = trim(preg_replace('/\s+/', ' ', "{$giftPackingGroupDelivery['address_country']} {$giftPackingGroupDelivery['address_city']} {$giftPackingGroupDelivery['address_street']} {$giftPackingGroupDelivery['address_zipcode']} {$giftPackingGroupDelivery['address_apartment_no']}"));
            }
            $giftPackingGroups[$key]['giftpacking_delivery_price'] = $deliveryCost;
            $giftPackingGroups[$key]['products'] = GiftPackingRepository::getProductsForGiftPackingGroup($giftPackingGroup['id']);
        }

        $this->context->smarty->assign([
            'admin_giftpackinggroups' => $giftPackingGroups,
            'hook' => 'hookDisplayAdminOrderMain'
        ]);

        return $this->display(__FILE__, 'admin_giftpacking_list.tpl');
    }

    public function displayGiftPacking()
    {
        $this->context->smarty->assign([
            'items' => $this->getFormItems(),
            'link' => $this->context->link,
            'groups' => $this->getGroups(),
            'images' => $this->getImages(),
            'packages' => GiftPackingModel::getAllList(true),
            'hasProductsWithoutPackingAndAddresses' => $this->hasProductsWithoutPackingAndAddresses($this->context->cart->id) ? '1' : '0',
            'carrier_id' => Configuration::get('SC_GIFT_PACKING_CARRIER_ID'),
        ]);
        return $this->display(__FILE__, 'views/templates/front/giftpacking.tpl');
    }

    public function displayGiftPackingDeliveryTable($giftPackingDeliverySelected = null)
    {
        $this->context->smarty->assign([
            'giftPackingDeliveryAddresses' => GiftPackingRepository::getAddressForCart($this->context->cart->id),
            'delivers' => GiftPackingDeliveryModel::getAllList(true),
        ]);
        if ($giftPackingDeliverySelected) {
            $this->context->smarty->assign([
                'giftPackingDeliverySelected' => $giftPackingDeliverySelected,
            ]);
        }
        return $this->display(__FILE__, 'views/templates/front/addresses.tpl');
    }

    public function displayGiftPackingAddressTable($giftPackingDeliverySelected = null)
    {
        $this->context->smarty->assign([
            'giftPackingDeliveryAddress' => GiftPackingRepository::getAddressForCart($this->context->cart->id, $giftPackingDeliverySelected) ?: null,
            'delivers' => GiftPackingDeliveryModel::getAllList(true),
        ]);
        return $this->display(__FILE__, 'views/templates/front/address.tpl');
    }

    public function getMessage($cartId)
    {
        $message = '';

        $giftPackingCart = GiftPackingRepository::getGiftGroupsForCart($cartId);

        if (!empty($giftPackingCart)) {
            $taxes = TaxRulesGroup::getAssociatedTaxRatesByIdCountry($this->context->country->id);
            foreach ($giftPackingCart as $index => $giftPacking) {
                $no = $index + 1;
                if (!empty($giftPacking['id_giftpacking'])) {
                    $giftPackingModel = GiftPackingModel::getAllOrderGiftByid($giftPacking['id_giftpacking']);
                    $packingTax = $taxes[$giftPackingModel['tax'] ?? 0] ?? 0;
                    $packageCost = $this->context->currentLocale->formatPrice(Tools::convertPrice(($giftPackingModel['price'] ?? 0) * (($packingTax + 100) / 100.)), $this->context->currency->iso_code);
                    if (!empty($giftPacking['different_delivery_address']) && $giftPacking['different_delivery_address'] != 0) {
                        $giftPackingDeliveryModel = GiftPackingRepository::getGiftPackingDeliveryById($giftPacking['id_giftpacking_group_delivery'], $cartId);
                        $deliveryTax = $taxes[$giftPackingDeliveryModel['tax'] ?? 0] ?? 0;
                        $deliveryCost = $this->context->currentLocale->formatPrice(Tools::convertPrice(($giftPackingDeliveryModel['price'] ?? 0) * (($deliveryTax + 100) / 100.)), $this->context->currency->iso_code);
                    }
                    $message .= "Prezent $no: {$giftPackingModel['title']} ($packageCost)";
                    if (isset($giftPackingDeliveryModel)) {
                        $giftPackingGroupDelivery = GiftPackingRepository::getGiftPackingGroupDeliveryById($giftPacking['id_giftpacking_group_delivery'], $cartId);
                        $message .= " | Dostawa: {$giftPackingDeliveryModel['title']} ($deliveryCost) - \"" . trim(preg_replace('/\s+/', ' ', "{$giftPackingGroupDelivery['address_country']} {$giftPackingGroupDelivery['address_city']} {$giftPackingGroupDelivery['address_street']} {$giftPackingGroupDelivery['address_zipcode']} {$giftPackingGroupDelivery['address_apartment_no']}")) . "\", {$giftPackingGroupDelivery['name_and_surname']}, {$giftPackingGroupDelivery['email']}, {$giftPackingGroupDelivery['phone']}";
                    }
                    $products = implode(', ', array_map(function ($product) {
                        return "{$product['product_name']} (ref: {$product['reference']}) ({$product['quantity']} szt.)";
                    }, GiftPackingRepository::getProductsForGiftPackingGroup($giftPacking['id'])));
                    $message .= " | Produkty: $products";
                    if (!empty($giftPacking['message'])) {
                        $message .= " | Wiadomość: \"" . trim(preg_replace('/\s+/', ' ', $giftPacking['message'])) . "\"";
                    }
                    if (count($giftPackingCart) - 1 > $index) {
                        $message .= "\n-----\n";
                    }
                }
            }
        }
        return $message;
    }

    public function calculateGiftPackingShippingCost()
    {
        $giftpackingFees = 0;
        $cartId = $this->context->cart->id ?? Tools::getValue('id_cart');
        $giftPackingCart = GiftPackingRepository::getGiftGroupsForCart($cartId);
        if (!empty($giftPackingCart)) {

            foreach ($giftPackingCart as $product) {
                if (!empty($product['different_delivery_address']) && $product['different_delivery_address'] != 0) {
                    $giftPackingDelivery = GiftPackingRepository::getGiftPackingDeliveryById($product['id_giftpacking_group_delivery'], $cartId);
                    $tax = TaxRulesGroup::getAssociatedTaxRatesByIdCountry($this->context->country->id)[$giftPackingDelivery['tax'] ?? 0] ?? 0;
                    $giftpackingFees += Tools::convertPrice(($giftPackingDelivery['price'] ?? 0) * (($tax + 100) / 100.));
                }
            }
        }
        return $giftpackingFees;
    }

    public function calculateGiftPackingFees($id_cart)
    {
        $giftpackingFees = 0;
        $giftPackingCart = GiftPackingRepository::getGiftGroupsForCart($id_cart);
        if (!empty($giftPackingCart)) {

            foreach ($giftPackingCart as $giftPacking) {
                if (!empty($giftPacking['id_giftpacking'])) {
                    $giftPackingModel = GiftPackingModel::getAllOrderGiftByid($giftPacking['id_giftpacking']);
                    $tax = TaxRulesGroup::getAssociatedTaxRatesByIdCountry($this->context->country->id)[$giftPackingModel['tax'] ?? 0] ?? 0;
                    $giftpackingFees += Tools::convertPrice(($giftPackingModel['price'] ?? 0) * (($tax + 100) / 100.));
                }
            }
        }
        return $giftpackingFees;
    }

    public function pagination($sql_select, $page = 1, $pagination = 50)
    {
        if (count($sql_select) > $pagination) {
            $sql_select = array_slice($sql_select, $pagination * ($page - 1), $pagination);
        }
        return $sql_select;
    }

    public function hasProductsWithoutPackingAndAddresses($cartId)
    {
        $cart = new Cart($cartId);
        $products = $cart->getProducts(true);
        $count = 0;
        foreach ($products as $product) {
            $count += $product['cart_quantity'] ?? 0;
        }
        $query = new DbQuery();
        $result = Db::getInstance()->executeS(
            $query
                ->select('gp_g.id')
                ->from('giftpacking_group', 'gp_g')
                ->innerJoin('giftpacking_group_product', 'gp_g_p', 'gp_g_p.id_giftpacking_group=gp_g.id')
                ->innerJoin('giftpacking_group_delivery', 'gp_g_d', 'gp_g_d.id=gp_g.id_giftpacking_group_delivery')
                ->where('gp_g.`id_cart` = ' . (int)$cartId)
                ->where('gp_g.`different_delivery_address` = 1')
                ->build()
        );
        return count($result) < $count;
    }

    public function hasAnyProduct($cartId)
    {
        $query = new DbQuery();
        $result = Db::getInstance()->executeS(
            $query
                ->select('gp_g.id')
                ->from('giftpacking_group', 'gp_g')
                ->innerJoin('giftpacking_group_product', 'gp_g_p', 'gp_g_p.id_giftpacking_group=gp_g.id')
                ->innerJoin('giftpacking_group_delivery', 'gp_g_d', 'gp_g_d.id=gp_g.id_giftpacking_group_delivery')
                ->where('gp_g.`id_cart` = ' . (int)$cartId)
                ->where('gp_g.`different_delivery_address` = 1')
                ->build()
        );
        return count($result) > 0;
    }

    public function getContent()
    {
        if (Tools::isSubmit('submit_settings_form')) {
            $this->postProcess();
        }
        $this->globalVars();
        return $this->displayForm();
    }

    public function displayForm()
    {
        $html = '';
        $content = '';
        $html .= $this->renderMenuTabs();
        switch (Tools::getValue($this->name)) {
            case 'list':
                $content .= $this->renderModalTpl();
                $content .= $this->renderList();
                break;
            case 'delivery':
                $content .= $this->renderModalTpl();
                $content .= $this->renderDeliveryList();
                break;
            case 'generel_settings':
            default:
                $content .= $this->renderSettingsForm();
        }
        $html .= $this->renderDisplayForm($content);

        $this->context->controller->addCSS($this->_path . 'views/css/admin.css', 'all');
        $this->context->controller->addJS(($this->_path) . 'views/js/admin.js');
        $html .= $this->renderModuleAdminVariables();
        return $html;
    }

    public function renderModalTpl()
    {
        $this->context->smarty->assign([
            'psv' => $this->psv,
        ]);
        return $this->display(__FILE__, 'views/templates/admin/modal.tpl');
    }

    public function renderDisplayForm($content)
    {
        $this->context->smarty->assign([
            'psv' => $this->psv,
            'errors' => $this->errors,
            'success' => $this->success,
            'content' => $content,
        ]);
        return $this->display(__FILE__, 'views/templates/admin/display_form.tpl');
    }

    private function globalVars()
    {
        $this->psv = (float)Tools::substr(_PS_VERSION_, 0, 3);
        $this->image_width = trim(Configuration::get('SC_GIFT_PACKING_IMAGE_WIDTH'));
        $this->image_height = trim(Configuration::get('SC_GIFT_PACKING_IMAGE_HEIGHT'));
    }

    private function getTranslations()
    {
        return [
            'gift_no' => $this->l('Gift no.'),
            'without_packing' => $this->l('Without packing'),
        ];
    }

    private function getImages()
    {
        $images = [];
        $allGiftPacking = GiftPackingModel::getAllList(true);
        foreach ($allGiftPacking as $giftpacking) {
            $images[$giftpacking['id']] = __PS_BASE_URI__ . "modules/" . $this->name . "/views/img/upload/small/" . $giftpacking['image'];
        }
        return $images;
    }

    private function getDeliveryImages()
    {
        $images = [];
        $allGiftPackingDelivery = GiftPackingDeliveryModel::getAllList(true);
        foreach ($allGiftPackingDelivery as $giftpackingDelivery) {
            $images[$giftpackingDelivery['id']] = __PS_BASE_URI__ . "modules/" . $this->name . "/views/img/upload/small/" . $giftpackingDelivery['logo'];
        }
        return $images;
    }

    public function saveList($id = null)
    {
        $languages = Language::getLanguages(false);
        $giftPacking = new GiftPackingModel($id);
        $giftPacking->active = Tools::getValue('active');
        $giftPacking->price = trim(Tools::getValue('price'));
        $giftPacking->tax = Tools::getValue('tax');
        foreach ($languages as $lang) {
            $giftPacking->title[$lang['id_lang']] = Tools::getValue('title_' . $lang['id_lang']);
        }
        $giftPacking->image = $this->uploadImage('image', $id, $this->image_width, $this->image_height);
        $giftPacking->save();
    }

    public function saveDelivery($id = null)
    {
        $languages = Language::getLanguages(false);
        $giftPackingDelivery = new GiftPackingDeliveryModel($id);
        $giftPackingDelivery->active = Tools::getValue('active');
        $giftPackingDelivery->price = trim(Tools::getValue('price'));
        $giftPackingDelivery->tax = Tools::getValue('tax');
        foreach ($languages as $lang) {
            $giftPackingDelivery->title[$lang['id_lang']] = Tools::getValue('title_' . $lang['id_lang']);
        }
        $giftPackingDelivery->logo = $this->uploadImage('logo', $id, $this->image_width, $this->image_height);
        $giftPackingDelivery->save();
    }

    public function uploadImage($name, $id = null, $width = null, $height = null)
    {
        if ($_FILES[$name]['name'] !== '') {
            $type = Tools::strtolower(Tools::substr(strrchr($_FILES[$name]['name'], '.'), 1));
            $imagesize = getimagesize($_FILES['' . $name . '']['tmp_name']);
            if (isset($_FILES[$name]) &&
                isset($_FILES[$name]['tmp_name']) &&
                !empty($_FILES[$name]['tmp_name']) &&
                !empty($imagesize) &&
                in_array(Tools::strtolower(Tools::substr(strrchr($imagesize['mime'], '/'), 1)), ['jpg', 'gif', 'bmp', 'jpeg', 'png']) &&
                in_array($type, ['jpg', 'gif', 'bmp', 'jpeg', 'png'])) {
                $temp_name = tempnam(_PS_TMP_IMG_DIR_, 'MF');
                $salt = sha1(microtime());
                if (ImageManager::validateUpload($_FILES[$name])) {
                    return true;
                } elseif (!$temp_name || !move_uploaded_file($_FILES[$name]['tmp_name'], $temp_name)) {
                    return false;
                } elseif (!ImageManager::resize($temp_name, dirname(__FILE__) . '/views/img/upload/original/' . Tools::hash($_FILES[$name]['name'] . $salt) . '.' . $type, null, null, $type)) {
                    return false;
                } elseif (!ImageManager::resize($temp_name, dirname(__FILE__) . '/views/img/upload/small/' . Tools::hash($_FILES[$name]['name'] . $salt) . '.' . $type, $width, $height, $type)) {
                    return false;
                }
                if (isset($temp_name)) {
                    unlink($temp_name);
                }
                $image = Tools::hash($_FILES[$name]['name'] . $salt) . '.' . $type;
                $giftpacking = new GiftPackingModel($id);
                if ($giftpacking->image) {
                    if (file_exists(_PS_MODULE_DIR_ . $this->name . '/views/img/upload/original/' . $giftpacking->image)) {
                        unlink(_PS_MODULE_DIR_ . $this->name . '/views/img/upload/original/' . $giftpacking->image);
                    } elseif (file_exists(_PS_MODULE_DIR_ . $this->name . '/views/img/upload/small/' . $giftpacking->image)) {
                        unlink(_PS_MODULE_DIR_ . $this->name . '/views/img/upload/small/' . $giftpacking->image);
                    }
                }
            }
        } else {
            $giftpacking = new GiftPackingModel($id);
            $image = $giftpacking->image;
        }
        return $image;
    }

    private function getFormItems()
    {
        $cart = new Cart($this->context->cart->id);
        $products = $cart->getProducts(true);
        $result = [
            'products' => [],
            'groups_count' => count(GiftPackingRepository::getGiftGroupsForCart($cart->id)),
        ];
        foreach ($products as $product) {
            for ($index = 0; $index < $product['cart_quantity']; $index++) {
                if ($product['is_virtual'] == 0) {
                    $groups = GiftPackingRepository::getGiftGroupNoByProductId($cart->id, $product['id_product']);
                    $result['products'][] = [
                        'product' => $product,
                        'group_no' => !empty($groups[$index]) ? $groups[$index]['group_no'] ?? null : null,
                    ];
                }
            }
        }

        return $result;
    }

    private function getGroups()
    {
        $cart = new Cart($this->context->cart->id);
        $result = [];
        $groups = GiftPackingRepository::getGiftGroupsForCart($cart->id);
        if (!empty($groups)) {
            foreach ($groups as $group) {
                $result[] = [
                    'group' => $group,
                    'products' => GiftPackingRepository::getProductsForGiftPackingGroup($group['id'])
                ];
            }
        }

        return $result;
    }

    public function renderListForm($type = '', $id_row = null)
    {
        $taxs = [];
        array_push($taxs, [
            'id' => 0,
            'name' => $this->l('None'),
        ]);
        foreach (TaxRulesGroup::getTaxRulesGroups() as $tax) {
            if ($tax['active'] == 1) {
                array_push($taxs, [
                    'id' => $tax['id_tax_rules_group'],
                    'name' => $tax['name'],
                ]);
            }
        }
        $image = '';
        if ($type == 'update') {
            $giftpacking = new GiftPackingModel($id_row);
            $image = $giftpacking->image;
        }
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ],
                'input' => [
                    [
                        'type' => 'hidden',
                        'name' => 'row_id',
                    ],
                    [
                        'type' => $this->psv >= 1.6 ? 'switch' : 'radio',
                        'label' => $this->l('Active'),
                        'name' => 'active',
                        'class' => 't',
                        'is_bool' => true,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ],
                            [
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            ]
                        ],
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Title'),
                        'name' => 'title',
                        'lang' => true,
                        'required' => true,
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Price'),
                        'name' => 'price',
                        'lang' => false,
                        'required' => true,
                        'prefix' => $this->context->currency->symbol . ' (' . $this->l('tax excl.') . ')',
                    ],
                    [
                        'type' => 'select',
                        'label' => $this->l('Tax'),
                        'name' => 'tax',
                        'options' => [
                            'query' => $taxs,
                            'id' => 'id',
                            'name' => 'name'
                        ]
                    ],
                    [
                        'type' => 'file',
                        'label' => $this->l('Image'),
                        'name' => 'image',
                        'required' => true,
                        'desc' => $image != '' ? "<img src='" . __PS_BASE_URI__ . "modules/" . $this->name . "/views/img/upload/small/" . $image . "' width='50'>" : '',
                    ],
                ],
                'submit' => [
                    'title' => ($type == 'update') ? $this->l('Update') : $this->l('Add'),
                    'name' => ($type == 'update') ? 'submit_list_update' : 'submit_list_add',
                    'class' => 'btn btn-default pull-right submit_item'
                ],
            ],
        ];

        $helper = new HelperForm();
        $languages = Language::getLanguages(false);
        foreach ($languages as $key => $language) {
            $languages[$key]['is_default'] = (int)($language['id_lang'] == Configuration::get('PS_LANG_DEFAULT'));
        }
        $helper->languages = $languages;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->show_toolbar = false;
        $this->fields_form = [];
        $helper->submit_action = 'submitBlockSettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name . '&' . $this->name . '=list';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->module = $this;

        $helper->tpl_vars = [
            'psv' => $this->psv,
            'fields_value' => $this->getListAddFieldsValues($type, $id_row)
        ];

        return $helper->generateForm([$fields_form]);
    }

    public function renderListDeliveryForm($type = '', $id_row = null)
    {
        $taxs = [];
        array_push($taxs, [
            'id' => 0,
            'name' => $this->l('None'),
        ]);
        foreach (TaxRulesGroup::getTaxRulesGroups() as $tax) {
            if ($tax['active'] == 1) {
                array_push($taxs, [
                    'id' => $tax['id_tax_rules_group'],
                    'name' => $tax['name'],
                ]);
            }
        }
        $logo = '';
        if ($type == 'update') {
            $giftpacking = new GiftPackingDeliveryModel($id_row);
            $logo = $giftpacking->logo;
        }
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ],
                'input' => [
                    [
                        'type' => 'hidden',
                        'name' => 'row_id',
                    ],
                    [
                        'type' => $this->psv >= 1.6 ? 'switch' : 'radio',
                        'label' => $this->l('Active'),
                        'name' => 'active',
                        'class' => 't',
                        'is_bool' => true,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ],
                            [
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            ]
                        ],
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Title'),
                        'name' => 'title',
                        'lang' => true,
                        'required' => true,
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Price'),
                        'name' => 'price',
                        'lang' => false,
                        'required' => true,
                        'prefix' => $this->context->currency->symbol . ' (' . $this->l('tax excl.') . ')',
                    ],
                    [
                        'type' => 'select',
                        'label' => $this->l('Tax'),
                        'name' => 'tax',
                        'options' => [
                            'query' => $taxs,
                            'id' => 'id',
                            'name' => 'name'
                        ]
                    ],
                    [
                        'type' => 'file',
                        'label' => $this->l('Logo'),
                        'name' => 'logo',
                        'required' => true,
                        'desc' => $logo != '' ? "<img src='" . __PS_BASE_URI__ . "modules/" . $this->name . "/views/img/upload/small/" . $logo . "' width='50'>" : '',
                    ],
                ],
                'submit' => [
                    'title' => ($type == 'update') ? $this->l('Update') : $this->l('Add'),
                    'name' => ($type == 'update') ? 'submit_list_update' : 'submit_list_add',
                    'class' => 'btn btn-default pull-right submit_item'
                ],
            ],
        ];

        $helper = new HelperForm();
        $languages = Language::getLanguages(false);
        foreach ($languages as $key => $language) {
            $languages[$key]['is_default'] = (int)($language['id_lang'] == Configuration::get('PS_LANG_DEFAULT'));
        }
        $helper->languages = $languages;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->show_toolbar = false;
        $this->fields_form = [];
        $helper->submit_action = 'submitBlockSettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name . '&' . $this->name . '=delivery';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->module = $this;

        $helper->tpl_vars = [
            'psv' => $this->psv,
            'fields_value' => $this->getListDeliveryAddFieldsValues($type, $id_row)
        ];

        return $helper->generateForm([$fields_form]);
    }

    public function getListAddFieldsValues($type = '', $id_row = null)
    {
        if ($type == 'update') {
            $list = new GiftPackingModel($id_row);
            return [
                'row_id' => $id_row,
                'active' => $list->active,
                'price' => $list->price,
                'tax' => $list->tax,
                'title' => $list->title,
            ];
        } else {
            $empty_array = [];
            foreach (Language::getLanguages(false) as $lang) {
                $empty_array[$lang['id_lang']] = '';
            }
            return [
                'row_id' => '',
                'active' => true,
                'price' => '',
                'tax' => 0,
                'title' => $empty_array,
            ];
        }
    }

    public function getListDeliveryAddFieldsValues($type = '', $id_row = null)
    {
        if ($type == 'update') {
            $list = new GiftPackingDeliveryModel($id_row);
            return [
                'row_id' => $id_row,
                'active' => $list->active,
                'price' => $list->price,
                'tax' => $list->tax,
                'title' => $list->title,
            ];
        } else {
            $empty_array = [];
            foreach (Language::getLanguages(false) as $lang) {
                $empty_array[$lang['id_lang']] = '';
            }
            return [
                'row_id' => '',
                'active' => true,
                'price' => '',
                'tax' => 0,
                'title' => $empty_array,
            ];
        }
    }

    public function renderModuleAdminVariables()
    {
        $moduleUrl = $this->context->link->getAdminLink('GiftPackingModule');
        $this->context->smarty->assign([
            'psv' => $this->psv,
            'id_lang' => $this->context->language->id,
            'sc_gift_packing_secure_key' => $this->secure_key,
            'sc_gift_packing_admin_controller_dir' => [
                'neutral' => $moduleUrl,
                'save_list' => "$moduleUrl&$this->name=list",
                'save_delivery' => "$moduleUrl&$this->name=delivery",
            ],
        ]);
        return $this->display(__FILE__, 'views/templates/admin/variables.tpl');
    }

    public function renderList()
    {
        $fields_list = [
            'id' => [
                'title' => $this->l('ID'),
                'width' => 60,
                'type' => 'text',
                'search' => false,
            ],
            'title' => [
                'title' => $this->l('Title'),
                'width' => 150,
                'type' => 'text',
                'search' => false,
            ],
            'price' => [
                'title' => $this->l('Price'),
                'width' => 140,
                'type' => 'text',
                'search' => false,
            ],
            'active' => [
                'title' => $this->l('Active'),
                'width' => 140,
                'type' => 'bool',
                'search' => false,
            ],
        ];
        $helper = new HelperList();
        $helper->module = $this;
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->no_link = true;
        $helper->actions = ['edit', 'delete'];
        $helper->identifier = 'id';
        $helper->show_toolbar = false;
        $helper->title = $this->l('List');
        $helper->table = 'giftpacking';
        $helper->toolbar_btn['new'] = [
            'href' => '#',
            'desc' => $this->l('Add')
        ];
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name . '&' . $this->name . '=list';
        $sql_result = GiftPackingRepository::getAllList();
        if (!empty($sql_result)) {
            foreach ($sql_result as $key => $value) {
                $sql_result[$key]['price'] = $this->context->currentLocale->formatPrice($value['price'], $this->context->currency->iso_code);
            }
        }
        $helper->listTotal = count($sql_result);
        $page = ($page = Tools::getValue('submitFilter' . $helper->table)) ? $page : 1;
        $pagination = ($pagination = Tools::getValue($helper->table . '_pagination')) ? $pagination : 50;
        $sql_result = $this->pagination($sql_result, $page, $pagination);
        return $helper->generateList($sql_result, $fields_list);
    }

    public function renderDeliveryList()
    {
        $fields_list = [
            'id' => [
                'title' => $this->l('ID'),
                'width' => 60,
                'type' => 'text',
                'search' => false,
            ],
            'title' => [
                'title' => $this->l('Title'),
                'width' => 150,
                'type' => 'text',
                'search' => false,
            ],
            'price' => [
                'title' => $this->l('Price'),
                'width' => 140,
                'type' => 'text',
                'search' => false,
            ],
            'active' => [
                'title' => $this->l('Active'),
                'width' => 140,
                'type' => 'bool',
                'search' => false,
            ],
        ];
        $helper = new HelperList();
        $helper->module = $this;
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->no_link = true;
        $helper->actions = ['edit', 'delete'];
        $helper->identifier = 'id';
        $helper->show_toolbar = false;
        $helper->title = $this->l('List');
        $helper->table = 'giftpacking_delivery';
        $helper->toolbar_btn['new'] = [
            'href' => '#',
            'desc' => $this->l('Add')
        ];
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name . '&' . $this->name . '=delivery';
        $sql_result = GiftPackingDeliveryModel::getAllList();
        if (!empty($sql_result)) {
            foreach ($sql_result as $key => $value) {
                $sql_result[$key]['price'] = $this->context->currentLocale->formatPrice($value['price'], $this->context->currency->iso_code);
            }
        }
        $helper->listTotal = count($sql_result);
        $page = ($page = Tools::getValue('submitFilter' . $helper->table)) ? $page : 1;
        $pagination = ($pagination = Tools::getValue($helper->table . '_pagination')) ? $pagination : 50;
        $sql_result = $this->pagination($sql_result, $page, $pagination);
        return $helper->generateList($sql_result, $fields_list);
    }

    private function renderSettingsForm()
    {
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->l('General settings'),
                    'icon' => 'icon-cogs'
                ],
                'input' => [
                    [
                        'type' => 'text',
                        'label' => $this->l('Image width'),
                        'name' => 'image_width',
                        'lang' => false,
                        'suffix' => 'px',
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Image height'),
                        'name' => 'image_height',
                        'lang' => false,
                        'suffix' => 'px',
                    ],
                ],
                'submit' => [
                    'title' => $this->l('Save'),
                    'name' => 'submit_settings_form',
                    'class' => $this->psv >= 1.6 ? 'btn btn-default pull-right' : 'button',
                ],
            ],
        ];

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $languages = Language::getLanguages(false);
        foreach ($languages as $key => $language) {
            $languages[$key]['is_default'] = (int)($language['id_lang'] == Configuration::get('PS_LANG_DEFAULT'));
        }
        $helper->languages = $languages;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $this->fields_form = [];
        $helper->submit_action = 'submitBlockSettings';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = $this->context->link->getAdminLink(
                'AdminModules',
                false
            ) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name . '&' . $this->name . '=generel_settings';
        $helper->tpl_vars = [
            'fields_value' => [
                'image_width' => $this->image_width,
                'image_height' => $this->image_height,
            ],
        ];
        return $helper->generateForm([$fields_form]);
    }

    private function renderMenuTabs()
    {
        $tabs = [
            'generel_settings' => [
                'title' => $this->l('General settings'),
                'icon' => 'icon-cog'
            ],
            'list' => [
                'title' => $this->l('List of items'),
                'icon' => 'icon-gift'
            ],
            'delivery' => [
                'title' => $this->l('Delivery'),
                'icon' => 'icon-truck'
            ],
        ];

        $this->context->smarty->assign([
            'psv' => $this->psv,
            'tabs' => $tabs,
            'module_version' => $this->version,
            'module_url' => $this->giftPackingModule->getModuleUrl(),
            'module_tab_key' => $this->name,
            'active_tab' => Tools::getValue($this->name),
            'logo_url' => __PS_BASE_URI__ . "modules/$this->name/views/img/sc-logo.png"
        ]);
        return $this->display(__FILE__, 'views/templates/admin/menu_tabs.tpl');
    }

    private function postProcess()
    {
        if (Tools::isSubmit('submit_settings_form')) {
            if (!Validate::isInt(Tools::getValue('image_width')) || Tools::getValue('image_width') == '') {
                $this->errors[] = $this->l('Invalid value for width');
            } elseif (!Validate::isInt(Tools::getValue('image_height')) || Tools::getValue('image_height') == '') {
                $this->errors[] = $this->l('Invalid value for height');
            }
            if (empty($this->errors)) {
                Configuration::updateValue('SC_GIFT_PACKING_IMAGE_WIDTH', trim(Tools::getValue('image_width')));
                Configuration::updateValue('SC_GIFT_PACKING_IMAGE_HEIGHT', trim(Tools::getValue('image_height')));
                $this->success[] = $this->l('Successful Save');
            }
        }
    }
}