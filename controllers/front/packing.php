<?php

class GiftPackingPackingModuleFrontController extends ModuleFrontController
{
    private $dbPrefix = _DB_PREFIX_;

    public function __construct()
    {
        $this->ajax = Tools::getValue('ajax');
        parent::__construct();
    }

    public function init()
    {
        parent::init();
        if ($this->ajax) {
            if (Tools::getValue('action') == 'set_gift_packing') {
                $this->updateGiftPacking();
                die(json_encode([
                    'success' => true,
                ]));
            }
            if (Tools::getValue('action') == 'get_gift_packing') {
                die(json_encode([
                    'success' => true,
                    'content' => [
                        'main' => $this->module->displayGiftPacking(),
                        'modal' => $this->module->displayGiftPackingDeliveryTable(Tools::getValue('giftpacking_delivery_selected', null)),
                    ],
                ]));
            }
            if (Tools::getValue('action') == 'get_gift_packing_address') {
                die(json_encode([
                    'success' => true,
                    'content' => [
                        'address' => $this->module->displayGiftPackingAddressTable(Tools::getValue('giftpacking_delivery_selected', null)),
                    ],
                ]));
            }
            if (Tools::getValue('action') == 'choose_giftpacking') {
                $this->chooseGiftPacking();
                die(json_encode([
                    'success' => true,
                ]));
            }
            if (Tools::getValue('action') == 'choose_giftpacking_delivery') {
                $this->chooseGiftpackingDelivery();
                die(json_encode([
                    'success' => true,
                ]));
            }
            if (Tools::getValue('action') == 'choose_giftpacking_delivery_address') {
                $this->chooseGiftpackingDeliveryAddress();
                die(json_encode([
                    'success' => true,
                ]));
            }
            if (Tools::getValue('action') == 'add_delivery_address') {
                $this->addDeliveryAddress();
                die(json_encode([
                    'success' => true,
                    'content' => $this->module->displayGiftPackingDeliveryTable(Tools::getValue('giftpacking_delivery_selected', null)),
                ]));
            }
            if (Tools::getValue('action') == 'update_message_gift') {
                $this->updateMessageGift();
                die(json_encode([
                    'success' => true,
                ]));
            }
            if (Tools::getValue('action') == 'set_status_different_delivery_address_giftpacking') {
                $this->setStatusDifferentDeliveryAddressGiftpacking();
                die(json_encode([
                    'success' => true,
                    'content' => $this->module->displayGiftPacking(),
                ]));
            }
            if (Tools::getValue('action') == 'set_giftpacking_delivery_address_name') {
                $this->setGiftpackingDeliveryAddressName();
                die(json_encode([
                    'success' => true,
                    'content' => $this->module->displayGiftPacking(),
                ]));
            }
            if (Tools::getValue('action') == 'set_giftpacking_delivery_address_country') {
                $this->setGiftpackingDeliveryAddressCountry();
                die(json_encode([
                    'success' => true,
                    'content' => $this->module->displayGiftPacking(),
                ]));
            }
            if (Tools::getValue('action') == 'set_giftpacking_delivery_address_city') {
                $this->setGiftpackingDeliveryAddressCity();
                die(json_encode([
                    'success' => true,
                    'content' => $this->module->displayGiftPacking(),
                ]));
            }
            if (Tools::getValue('action') == 'set_giftpacking_delivery_address_zipcode') {
                $this->setGiftpackingDeliveryAddressZipcode();
                die(json_encode([
                    'success' => true,
                    'content' => $this->module->displayGiftPacking(),
                ]));
            }
            if (Tools::getValue('action') == 'set_giftpacking_delivery_address_street') {
                $this->setGiftpackingDeliveryAddressStreet();
                die(json_encode([
                    'success' => true,
                    'content' => $this->module->displayGiftPacking(),
                ]));
            }
            if (Tools::getValue('action') == 'set_giftpacking_delivery_address_apartment_no') {
                $this->setGiftpackingDeliveryAddressApartmentNo();
                die(json_encode([
                    'success' => true,
                    'content' => $this->module->displayGiftPacking(),
                ]));
            }
            if (Tools::getValue('action') == 'set_giftpacking_delivery_name_and_surname') {
                $this->setGiftpackingDeliveryNameAndSurname();
                die(json_encode([
                    'success' => true,
                    'content' => $this->module->displayGiftPacking(),
                ]));
            }
            if (Tools::getValue('action') == 'set_giftpacking_delivery_email') {
                $this->setGiftpackingDeliveryEmail();
                die(json_encode([
                    'success' => true,
                    'content' => $this->module->displayGiftPacking(),
                ]));
            }
            if (Tools::getValue('action') == 'set_giftpacking_delivery_phone') {
                $this->setGiftpackingDeliveryPhone();
                die(json_encode([
                    'success' => true,
                    'content' => $this->module->displayGiftPacking(),
                ]));
            }
            if (Tools::getValue('action') == 'delete_giftpacking_delivery') {
                $this->deleteGiftpackingDeliveryAddress();
                die(json_encode([
                    'success' => true,
                    'content' => $this->module->displayGiftPacking(),
                ]));
            }
        }
    }

    private function updateGiftPacking()
    {
        $giftGroups = GiftPackingRepository::getGiftGroupsForCart($this->context->cart->id);
        $giftPackages = Tools::getValue('giftPackages', []);
        foreach ($giftGroups as $index => $giftGroup) {
            $delete = true;
            foreach ($giftPackages as $giftPackage) {
                if ($giftPackage['group_no'] == $giftGroup['group_no']) {
                    $delete = false;
                }
            }
            if ($delete) {
                Db::getInstance()->execute(<<<SQL
DELETE FROM `{$this->dbPrefix}giftpacking_group_product` WHERE id_giftpacking_group={$giftGroup['id']}
SQL
                );
                Db::getInstance()->execute(<<<SQL
DELETE FROM `{$this->dbPrefix}giftpacking_group` WHERE id={$giftGroup['id']}
SQL
                );
                unset($giftGroups[$index]);
            }
        }
        foreach ($giftGroups as $giftGroup) {
            Db::getInstance()->execute(<<<SQL
DELETE FROM `{$this->dbPrefix}giftpacking_group_product` WHERE id_giftpacking_group={$giftGroup['id']}
SQL
            );
        }
        $numOfGroups = !empty($giftPackages) ? max(array_map(function ($giftPackage) {
            return $giftPackage['group_no'] ?? 0;
        }, $giftPackages)) : 0;
        $giftGroups = [];
        $allGiftPacking = GiftPackingModel::getAllList(true);
        $firstGiftPacking = reset($allGiftPacking);
        $firstGiftPackingId = $firstGiftPacking['id'] ?? 'NULL';
        for ($index = 1; $index <= $numOfGroups; $index++) {
            $giftGroup = GiftPackingRepository::getGiftGroup($this->context->cart->id, $index);
            if (empty($giftGroup)) {
                Db::getInstance()->execute(<<<SQL
INSERT INTO `{$this->dbPrefix}giftpacking_group` (`id_cart`, `group_no`, `id_giftpacking`)
VALUES({$this->context->cart->id}, $index, $firstGiftPackingId)
SQL
                );
                $giftGroup = GiftPackingRepository::getGiftGroup($this->context->cart->id, $index);
            }
            if (!empty($giftGroup)) {
                $giftGroups[$index] = reset($giftGroup);
            }
        }
        foreach ($giftPackages as $giftPackage) {
            Db::getInstance()->execute(<<<SQL
INSERT INTO `{$this->dbPrefix}giftpacking_group_product` (`id_giftpacking_group`, `id_product`)
VALUES({$giftGroups[$giftPackage['group_no']]['id']}, {$giftPackage['product_id']})
SQL
            );
        }
    }

    private function chooseGiftPacking()
    {
        $idGiftPacking = addslashes(Tools::getValue('value')) ?: 'null';
        $groupNo = addslashes(Tools::getValue('group_no'));
        return Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group`
SET id_giftpacking=$idGiftPacking
WHERE id_cart={$this->context->cart->id} AND group_no={$groupNo}
SQL
        );
    }

    private function updateMessageGift()
    {
        $message = addslashes(Tools::getValue('value'));
        $groupNo = addslashes(Tools::getValue('group_no'));
        return Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group`
SET message="$message"
WHERE id_cart={$this->context->cart->id} AND group_no={$groupNo}
SQL
        );
    }

    private function setStatusDifferentDeliveryAddressGiftpacking()
    {
        $value = intval(Tools::getValue('value') === 'true');
        $groupNo = addslashes(Tools::getValue('group_no'));
        return Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group`
SET different_delivery_address=$value
WHERE id_cart={$this->context->cart->id} AND group_no=$groupNo
SQL
        );
    }

    private function setGiftpackingDeliveryAddressName()
    {
        $addressName = addslashes(Tools::getValue('value')) ?: '';
        $idGiftpackingDeliveryGroup = addslashes(Tools::getValue('id_giftpacking_delivery_group'));
        Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group_delivery`
SET address_name='$addressName'
WHERE id={$idGiftpackingDeliveryGroup}
SQL
        );
    }

    private function setGiftpackingDeliveryAddressCountry()
    {
        $addressCountry = addslashes(Tools::getValue('value')) ?: '';
        $idGiftpackingDeliveryGroup = addslashes(Tools::getValue('id_giftpacking_delivery_group'));
        Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group_delivery`
SET address_country='$addressCountry'
WHERE id={$idGiftpackingDeliveryGroup}
SQL
        );
    }

    private function setGiftpackingDeliveryAddressCity()
    {
        $addressCity = addslashes(Tools::getValue('value')) ?: '';
        $idGiftpackingDeliveryGroup = addslashes(Tools::getValue('id_giftpacking_delivery_group'));
        Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group_delivery`
SET address_city='$addressCity'
WHERE id={$idGiftpackingDeliveryGroup}
SQL
        );
    }

    private function setGiftpackingDeliveryAddressZipcode()
    {
        $addressZipcode = addslashes(Tools::getValue('value')) ?: '';
        $idGiftpackingDeliveryGroup = addslashes(Tools::getValue('id_giftpacking_delivery_group'));
        Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group_delivery`
SET address_zipcode='$addressZipcode'
WHERE id={$idGiftpackingDeliveryGroup}
SQL
        );
    }

    private function setGiftpackingDeliveryAddressStreet()
    {
        $addressStreet = addslashes(Tools::getValue('value')) ?: '';
        $idGiftpackingDeliveryGroup = addslashes(Tools::getValue('id_giftpacking_delivery_group'));
        Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group_delivery`
SET address_street='$addressStreet'
WHERE id={$idGiftpackingDeliveryGroup}
SQL
        );
    }

    private function setGiftpackingDeliveryAddressApartmentNo()
    {
        $addressApartmentNo = addslashes(Tools::getValue('value')) ?: '';
        $idGiftpackingDeliveryGroup = addslashes(Tools::getValue('id_giftpacking_delivery_group'));
        Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group_delivery`
SET address_apartment_no='$addressApartmentNo'
WHERE id={$idGiftpackingDeliveryGroup}
SQL
        );
    }

    private function setGiftpackingDeliveryNameAndSurname()
    {
        $nameAndSurname = addslashes(Tools::getValue('value')) ?: '';
        $idGiftpackingDeliveryGroup = addslashes(Tools::getValue('id_giftpacking_delivery_group'));
        Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group_delivery`
SET name_and_surname='$nameAndSurname'
WHERE id={$idGiftpackingDeliveryGroup}
SQL
        );
    }

    private function setGiftpackingDeliveryEmail()
    {
        $email = addslashes(Tools::getValue('value')) ?: '';
        $idGiftpackingDeliveryGroup = addslashes(Tools::getValue('id_giftpacking_delivery_group'));
        Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group_delivery`
SET email ='$email'
WHERE id={$idGiftpackingDeliveryGroup}
SQL
        );
    }

    private function setGiftpackingDeliveryPhone()
    {
        $phone = addslashes(Tools::getValue('value')) ?: '';
        $idGiftpackingDeliveryGroup = addslashes(Tools::getValue('id_giftpacking_delivery_group'));
        Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group_delivery`
SET phone ='$phone'
WHERE id={$idGiftpackingDeliveryGroup}
SQL
        );
    }

    private function deleteGiftpackingDeliveryAddress()
    {
        $idGiftpackingDeliveryGroup = addslashes(Tools::getValue('id_giftpacking_delivery_group'));
        Db::getInstance()->execute(<<<SQL
DELETE FROM `{$this->dbPrefix}giftpacking_group_delivery`
WHERE id=$idGiftpackingDeliveryGroup
SQL
        );
        return Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group`
SET id_giftpacking_group_delivery=null
WHERE id_giftpacking_group_delivery=$idGiftpackingDeliveryGroup
SQL
        );
    }

    private function addDeliveryAddress()
    {
        $allGiftPackingDelivery = GiftPackingDeliveryModel::getAllList(true);
        $firstGiftPackingDelivery = reset($allGiftPackingDelivery);
        $firstGiftPackingDeliveryId = $firstGiftPackingDelivery['id'] ?? 'NULL';
        return Db::getInstance()->execute(<<<SQL
INSERT INTO `{$this->dbPrefix}giftpacking_group_delivery` (`id_cart`, `id_giftpacking_delivery`)
VALUES({$this->context->cart->id}, $firstGiftPackingDeliveryId)
SQL
        );
    }

    private function chooseGiftpackingDeliveryAddress()
    {
        $idGiftpackingDeliveryGroup = addslashes(Tools::getValue('value')) ?: 'null';
        $groupNo = addslashes(Tools::getValue('group_no'));
        return Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group`
SET id_giftpacking_group_delivery=$idGiftpackingDeliveryGroup
WHERE id_cart={$this->context->cart->id} AND group_no=$groupNo
SQL
        );
    }

    private function chooseGiftpackingDelivery()
    {
        $idGiftpackingDelivery = addslashes(Tools::getValue('value')) ?: 'null';
        $idGiftpackingDeliveryGroup = addslashes(Tools::getValue('id_giftpacking_delivery_group'));
        return Db::getInstance()->execute(<<<SQL
UPDATE `{$this->dbPrefix}giftpacking_group_delivery`
SET id_giftpacking_delivery=$idGiftpackingDelivery
WHERE id={$idGiftpackingDeliveryGroup}
SQL
        );
    }
}