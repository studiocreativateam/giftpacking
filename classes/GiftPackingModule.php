<?php

class GiftPackingModule
{
    /**
     * @var $module giftpacking
     */
    private $module;

    public function __construct($module)
    {
        $this->module = $module;
        $this->context = Context::getContext();
    }

    public function createSubmenu()
    {
        $parentId = (int) Tab::getIdFromClassName('AdminCatalog');
        $className = self::class;

        $subTab = Tab::getInstanceFromClassName($className);
        if (!Validate::isLoadedObject($subTab)) {
            $subTab->active = 1;
            $subTab->class_name = $className;
            $subTab->id_parent = $parentId;
            $subTab->module = $this->module->name;
            $languages = Language::getLanguages(false);
            foreach ($languages as $language) {
                $subTab->name[$language['id_lang']] = $this->module->l('Gift packing', false, $language['locale']);
            }
            return $subTab->save();
        }
        if ($subTab->id_parent != $parentId) {
            $subTab->id_parent = $parentId;
            return $subTab->save();
        }
        return true;
    }

    /**
     * @return bool
     * @throws PrestaShopException
     */
    public function deleteSubmenu()
    {
        $subTab = Tab::getInstanceFromClassName(self::class);
        return $subTab->delete();
    }

    /**
     * @param string $prefix (adding in url parameter name)
     * @return string url
     * @throws PrestaShopException
     */
    public function getModuleUrl($prefix = '')
    {
        $module_token = $this->context->link->getAdminLink('AdminModules') . '&configure=' . $this->module->name . '&tab_module=' . $this->module->tab . '&module_name=' . $this->module->name . $prefix;
        if ($this->module->psv >= 1.7) {
            return $module_token;
        }
        if ($this->module->psv == 1.6) {
            $host_webpath = Tools::getAdminUrl() . $this->context->controller->admin_webpath;
        } else {
            $host_webpath = Tools::getHttpHost(true) . __PS_BASE_URI__
                . Tools::substr(str_ireplace(_PS_ROOT_DIR_, '', getcwd()), 1);
        }
        return $host_webpath . '/' . $module_token;
    }
}