<?php

class GiftPackingModel extends ObjectModel
{
    public $id;
    public $active;
    public $title;
    public $price;
    public $tax;
    public $image;

    public static $definition = [
        'table' => 'giftpacking',
        'primary' => 'id',
        'multilang' => true,
        'fields' => [
            'active' => [
                'type' => self::TYPE_INT, 'validate' => 'isInt'],
            'title' => [
                'type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 255, 'lang' => true],
            'price' => [
                'type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 100],
            'tax' => [
                'type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 100],
            'image' => [
                'type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'],
        ],
    ];

    public function add($autodate = true, $null_values = false)
    {
        $context = Context::getContext();

        $res = parent::add($autodate, $null_values);
        $res &= Db::getInstance()->execute('
            INSERT INTO `' . _DB_PREFIX_ . 'giftpacking_shop` (`id_giftpacking`, `id_shop`)
            VALUES(' . (int)$this->id . ', ' . (int)$context->shop->id . ')'
        );

        return $res;
    }

    public function delete()
    {
        $res = parent::delete();
        $res &= Db::getInstance()->execute('
            DELETE FROM `' . _DB_PREFIX_ . 'giftpacking`
            WHERE id = ' . (int)$this->id);

        $res &= Db::getInstance()->execute('
            DELETE FROM `' . _DB_PREFIX_ . 'giftpacking_shop` 
            WHERE id_giftpacking = ' . (int)$this->id);

        return $res;
    }

    public static function getAllList($active = false)
    {
        $id_shop = Context::getContext()->shop->id;
        $active_in = $active ? '("1")' : '("0","1")';
        $query = new DbQuery();
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS(
            $query
                ->select('gp.*')
                ->select('gp_l.*')
                ->from('giftpacking', 'gp')
                ->leftJoin('giftpacking_lang', 'gp_l', 'gp_l.`id` = gp.`id`')
                ->leftJoin('giftpacking_shop', 'gp_s', 'gp_s.`id_giftpacking` = gp.`id`')
                ->where('gp_l.`id_lang` = ' . (int)Context::getContext()->language->id)
                ->where('gp_s.`id_shop` = ' . $id_shop)
                ->where('gp.active IN ' . $active_in)
                ->build()
        );
        return $result;
    }

    public static function getAllOrderGiftByid($id)
    {
        $query = new DbQuery();
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow(
            $query
                ->select('gp.*')
                ->select('gp_l.*')
                ->from('giftpacking', 'gp')
                ->leftJoin('giftpacking_lang', 'gp_l', 'gp_l.`id` = gp.`id`')
                ->where('gp_l.`id_lang` = ' . (int)Context::getContext()->language->id)
                ->where('gp.`id` = ' . (int)$id)
                ->build()
        );
        return $result;
    }
}