<?php

class GiftPackingDeliveryModel extends ObjectModel
{
    public $id;
    public $active;
    public $title;
    public $price;
    public $tax;
    public $logo;

    public static $definition = [
        'table' => 'giftpacking_delivery',
        'primary' => 'id',
        'multilang' => true,
        'fields' => [
            'active' => [
                'type' => self::TYPE_INT, 'validate' => 'isInt'],
            'title' => [
                'type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 255, 'lang' => true],
            'price' => [
                'type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 100],
            'tax' => [
                'type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 100],
            'logo' => [
                'type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'],
        ],
    ];

    public function add($autodate = true, $null_values = false)
    {
        $context = Context::getContext();

        $res = parent::add($autodate, $null_values);
        $res &= Db::getInstance()->execute('
            INSERT INTO `' . _DB_PREFIX_ . 'giftpacking_delivery_shop` (`id_giftpacking_delivery`, `id_shop`)
            VALUES(' . (int)$this->id . ', ' . (int)$context->shop->id . ')'
        );

        return $res;
    }

    public function delete()
    {
        $res = parent::delete();
        $res &= Db::getInstance()->execute('
            DELETE FROM `' . _DB_PREFIX_ . 'giftpacking_delivery`
            WHERE id = ' . (int)$this->id);

        $res &= Db::getInstance()->execute('
            DELETE FROM `' . _DB_PREFIX_ . 'giftpacking_delivery_shop` 
            WHERE id_giftpacking_delivery = ' . (int)$this->id);

        return $res;
    }

    public static function getAllList($active = false)
    {
        $id_shop = Context::getContext()->shop->id;
        $active_in = $active ? '("1")' : '("0","1")';
        $query = new DbQuery();
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS(
            $query
                ->select('gp_d.*')
                ->select('gp_d_l.*')
                ->from('giftpacking_delivery', 'gp_d')
                ->leftJoin('giftpacking_delivery_lang', 'gp_d_l', 'gp_d_l.`id` = gp_d.`id`')
                ->leftJoin('giftpacking_delivery_shop', 'gp_d_s', 'gp_d_s.`id_giftpacking_delivery` = gp_d.`id`')
                ->where('gp_d_l.`id_lang` = ' . (int)Context::getContext()->language->id)
                ->where('gp_d_s.`id_shop` = ' . $id_shop)
                ->where('gp_d.active IN ' . $active_in)
                ->build()
        );
        return $result;
    }

    public static function getAllOrderGiftByid($id)
    {
        $query = new DbQuery();
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow(
            $query
                ->select('gp_d.*')
                ->select('gp_d_l.*')
                ->from('giftpacking_delivery', 'gp_d')
                ->leftJoin('giftpacking_delivery_lang', 'gp_d_l', 'gp_d_l.`id` = gp_d.`id`')
                ->where('gp_d_l.`id_lang` = ' . (int)Context::getContext()->language->id)
                ->where('gp_d.`id` = ' . (int)$id)
                ->build()
        );
        return $result;
    }
}