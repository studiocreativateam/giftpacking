<?php

class GiftPackingRepository
{
    public static function getGiftGroupsForCart($cartId)
    {
        $query = new DbQuery();
        $result = Db::getInstance()->executeS(
            $query
                ->select('DISTINCT(gpg.id), gpg.*, gp_g_d.address_name, gp_d.price as delivery_price, gp_d.tax as delivery_tax, gp_d_l.title as delivery_title')
                ->from('giftpacking_group', 'gpg')
                ->where('gpg.`id_cart` = ' . (int)$cartId)
                ->innerJoin('giftpacking_group_product', 'gpgp', 'gpgp.id_giftpacking_group=gpg.id')
                ->leftJoin('giftpacking_group_delivery', 'gp_g_d', 'gp_g_d.id=gpg.id_giftpacking_group_delivery')
                ->leftJoin('giftpacking_delivery', 'gp_d', 'gp_g_d.id_giftpacking_delivery=gp_d.id')
                ->leftJoin('giftpacking_delivery_lang', 'gp_d_l', 'gp_d.id=gp_d_l.id AND gp_d_l.`id_lang` = ' . (int)Context::getContext()->language->id)
                ->innerJoin('cart_product', 'cp', 'cp.id_product=gpgp.id_product')
                ->build()
        );
        return $result;
    }

    public static function getGiftDeliveryForCart($cartId)
    {
        $query = new DbQuery();
        $result = Db::getInstance()->executeS(
            $query
                ->select('DISTINCT(gpg.id), gpd.*, gpg.different_delivery_address')
                ->from('giftpacking_group', 'gpg')
                ->innerJoin('giftpacking_group_delivery', 'gp_g_d', 'gp_g_d.id=gp_g.id_giftpacking_group_delivery')
                ->where('gpg.`id_cart` = ' . (int)$cartId)
                ->innerJoin('giftpacking_group_product', 'gpgp', 'gpgp.id_giftpacking_group=gpg.id')
                ->innerJoin('cart_product', 'cp', 'cp.id_product=gpgp.id_product')
                ->innerJoin('giftpacking_delivery', 'gpd', 'gpd.id=gp_g_d.id_giftpacking_delivery')
                ->where('cp.id_cart=gpg.id_cart')
                ->build()
        );
        return $result;
    }

    public static function getGiftGroup($cartId, $groupNo)
    {
        $query = new DbQuery();
        $result = Db::getInstance()->executeS(
            $query
                ->select('gpg.*')
                ->from('giftpacking_group', 'gpg')
                ->where('gpg.`id_cart` = ' . (int)$cartId)
                ->where('gpg.`group_no` = ' . (int)$groupNo)
                ->build()
        );
        return $result;
    }

    public static function getGiftGroupNoByProductId($cartId, $productId)
    {
        $query = new DbQuery();
        $result = Db::getInstance()->executeS(
            $query
                ->select('gpg.group_no')
                ->from('giftpacking_group', 'gpg')
                ->where('gpg.`id_cart` = ' . (int)$cartId)
                ->innerJoin('giftpacking_group_product', 'gpgp', 'gpgp.id_giftpacking_group=gpg.id')
                ->where('gpgp.`id_product` = ' . (int)$productId)
                ->build()
        );
        return $result;
    }

    public static function getAllList($active = false)
    {
        $id_shop = Context::getContext()->shop->id;
        $active_in = $active ? '("1")' : '("0","1")';
        $query = new DbQuery();
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS(
            $query
                ->select('gp.*')
                ->select('gp_l.*')
                ->from('giftpacking', 'gp')
                ->leftJoin('giftpacking_lang', 'gp_l', 'gp_l.`id` = gp.`id`')
                ->leftJoin('giftpacking_shop', 'gp_s', 'gp_s.`id_giftpacking` = gp.`id`')
                ->where('gp_l.`id_lang` = ' . (int)Context::getContext()->language->id)
                ->where('gp_s.`id_shop` = ' . $id_shop)
                ->where('gp.active IN ' . $active_in)
                ->build()
        );
        return $result;
    }

    public static function getProductsForGiftPackingGroup($giftPackingGroupId)
    {
        $query = new DbQuery();
        $result = Db::getInstance()->executeS(
            $query
                ->select('p.id_product, p.*, pl.name as product_name, COUNT(p.id_product) as quantity')
                ->from('product', 'p')
                ->innerJoin('product_lang', 'pl', 'pl.id_product=p.id_product')
                ->innerJoin('giftpacking_group_product', 'gp_g_p', 'gp_g_p.id_product=p.id_product')
                ->where('pl.`id_lang` = ' . (int)Context::getContext()->language->id)
                ->where('gp_g_p.`id_giftpacking_group` = ' . (int)$giftPackingGroupId)
                ->groupBy('p.id_product')
                ->build()
        );
        return $result;
    }

    public static function getAddressForCart($cartId, $giftpackingGroupDeliveryId = null)
    {
        $query = new DbQuery();
        $query
            ->select('gp_g_d.*')
            ->from('giftpacking_group_delivery', 'gp_g_d')
            ->where('gp_g_d.`id_cart` = ' . (int)$cartId);
        if ($giftpackingGroupDeliveryId) {
            $query->where('gp_g_d.`id` = ' . (int)$giftpackingGroupDeliveryId);
        }
        $result = Db::getInstance()->executeS(
            $query->build()
        );
        if ($giftpackingGroupDeliveryId) {
            $result = reset($result);
        }
        return $result;
    }

    public static function getGiftPackingGroupDeliveryById($giftpackingGroupDeliveryId, $cartId)
    {
        $query = new DbQuery();
        $result = Db::getInstance()->executeS(
            $query
                ->select('gp_g_d.*')
                ->from('giftpacking_group_delivery', 'gp_g_d')
                ->where('gp_g_d.`id` = ' . (int)$giftpackingGroupDeliveryId)
                ->build()
        );
        $result = reset($result);
        return $result;
    }

    public static function getGiftPackingDeliveryById($giftpackingGroupDeliveryId, $cartId)
    {
        $result = self::getGiftPackingGroupDeliveryById($giftpackingGroupDeliveryId, $cartId);
        if (!empty($result['id_giftpacking_delivery'])) {
            $giftpackingDelivery = new GiftPackingDeliveryModel($result['id_giftpacking_delivery'], Context::getContext()->language->id);

            if ($giftpackingDelivery) {
                return (array)$giftpackingDelivery;
            }
        }
        return null;
    }
}
